<?php
class Insurance_model extends CI_Model {

    var $insurance_id;
    var $insurance_name;
    var $exam_price;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["insurance_id"] = $this->insurance_id;
        $data["insurance_name"] = $this->insurance_name;
        $data["exam_price"] = $this->exam_price;
        
        return $data;
    }

    function load_insurance($insurance){

        $new_insurance = new Insurance_model();

        $new_insurance->insurance_id = $insurance->insurance_id;
        $new_insurance->insurance_name = $insurance->insurance_name;
        $new_insurance->exam_price = $insurance->exam_price;

        return $new_insurance;
    }

    function get_insurance($_insurance_id){

        $sql = "SELECT * FROM Insurance_type WHERE insurance_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($_insurance_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $this->insurance_id = $row->insurance_id;
            $this->insurance_name = $row->name;
            $this->exam_price = $row->exam_price;
            
        }
        return $this;
    }

    function get_insurance_types(){
        $sql = "SELECT * FROM Insurance_type";
        $query = $this->db->query($sql);

        $result_array = array();

        foreach($query->result() as $row){

            $insurance = $this->load_insurance($row);
            $result_array[] = $insurance;
            
        }
        return $result_array;
    }

    function insert_insurance($data)
    {
        $sql = "INSERT INTO Insurance_type (insurance_name, exam_price)" .
                    " values " . 
                    "(?, ?)";
        $this->db->query($sql, array($data["insurance_name"],
                                     $data["exam_price"]));
    }
    

}
?>