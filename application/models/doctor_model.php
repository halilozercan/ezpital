<?php
class Doctor_model extends CI_Model {

    var $doctor_id;
    var $email;
    var $name;
    var $surname;
    var $r_id;
    var $picture_url;
    var $title;
    var $department_id;

    public $room;
    public $department;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["doctor_id"] = $this->doctor_id;
        $data["email"] = $this->email;
        $data["name"] = $this->enter_date;
        $data["surname"] = $this->end_time;
        $data["r_id"] = $this->end_date;
        $data["picture_url"] = $this->reason;
        $data["title"] = $this->p_id;
        $data["department_id"] = $this->d_id;

        if( isset($this->room) ){
            //$data["room"]  = $this->room->as_array();
            $data["department"] = $this->department->as_array();
        }

        return $data;
    }

    function load_doctor($doctor){

        $new_doctor = new Doctor_model();

        $new_doctor->doctor_id = $doctor->user_id;
        $new_doctor->email = $doctor->email;
        $new_doctor->name = $doctor->name;
        $new_doctor->surname = $doctor->surname;
        $new_doctor->r_id = $doctor->r_id;
        $new_doctor->picture_url = $doctor->picture_url;
        $new_doctor->department_id = $doctor->department_id;
        $new_doctor->title = $doctor->title;

        return $new_doctor;
    }

    function get_doctor($_doctor_id){

        $this->load->model('department_model', '', TRUE);

        $sql = "SELECT * FROM User, Doctor, Department WHERE User.user_id=Doctor.d_id AND Doctor.department_id=Department.department_id AND User.user_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($_doctor_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $this->doctor_id = $row->user_id;
            $this->email = $row->email;
            $this->name = $row->name;
            $this->surname = $row->surname;
            $this->r_id = $row->r_id;
            $this->picture_url = $row->picture_url;
            $this->department_id = $row->department_id;
            $this->title = $row->title;

            $this->department = $this->department_model->load_department($row);
        }

        return $this;
    }

    function get_today_patient_count(){

        $sql = "SELECT Count(*) as total FROM Treatment NATURAL JOIN Examination WHERE Treatment.doctor_id = ? AND Examination.date = ?";
        $query = $this->db->query($sql, array($this->doctor_id, date("Y-m-d")));

        return $query->first_row()->total;
    }

    function get_week_patient_count(){

        $sql = "SELECT Count(*) as total FROM Treatment NATURAL JOIN Examination WHERE Treatment.doctor_id = ? AND Examination.date BETWEEN ? AND ?";
        $query = $this->db->query($sql, array($this->doctor_id, date("Y-m-d", strtotime("-7 DAYS")),date("Y-m-d")));

        return $query->first_row()->total;
    }

    function get_remaining_appointment_count(){

        $sql = "SELECT Count(*) as total FROM Appointment WHERE d_id = ? AND date = ?";
        $query = $this->db->query($sql, array($this->doctor_id, date("Y-m-d")));

        return $query->first_row()->total;
    }

    function get_last_visits($cap){

        $this->load->model('visit_model', '', TRUE);
        $this->load->model('patient_model', '', TRUE);

        $sql = "SELECT * FROM Visit , Patient WHERE Visit.p_id=Patient.patient_id AND Visit.d_id = ? AND end_time IS NOT NULL ORDER BY Visit.visit_id DESC LIMIT ?";
        $query = $this->db->query($sql, array($this->doctor_id, $cap));

        $visits = array();

        foreach($query->result() as $row){
            $visit = $this->visit_model->load_visit($row);
            $visit->patient = $this->patient_model->load_patient($row);
            $visit->is_active = !isset($visit->end_date);

            if($visit->is_active){
                $visit->is_active = "Yes";
            }
            else{
                $visit->is_active = "No";
            }

            $sql = "SELECT Count(*) as total FROM Analysis NATURAL JOIN Examination WHERE Examination.v_id = ? AND Analysis.report IS NULL";
            $query = $this->db->query($sql, array($visit->visit_id));

            $visit->waiting_analysis = "No";
            if($query->first_row()->total > 0){
                $visit->waiting_analysis = "Yes";
            }

            array_push($visits, $visit);
        }

        return $visits;

    }

    function get_available_doctors($department_id, $week_day = NULL, $time = NULL){

        $available_doctors = array();

        $this->load->model("visit_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);

        if( !isset($week_day) ){
            $week_day = date("N");
        }
        if( !isset($time) ){
            $time = date("h:i:s");
        }

        $sql = "SELECT * FROM Works_at, Doctor, User WHERE day = ? AND start_time < ? AND end_time > ? AND Works_at.u_id = Doctor.d_id AND Doctor.d_id=User.user_id AND Doctor.department_id = ?";
        $query = $this->db->query($sql, array($week_day, $time, $time, $department_id));

        foreach($query->result() as $working_doctor){

            $sql = "SELECT Count(*) as total FROM Visit WHERE enter_date = ? AND enter_time > ? AND enter_time < ? AND d_id = ?";
            $query = $this->db->query($sql, array(date('Y-m-d'), $working_doctor->start_time, $working_doctor->end_time, $working_doctor->u_id));

            $count = intval($query->first_row()->total);

            $sql = "SELECT Count(*) as total FROM Appointment WHERE date = ? AND time > ? AND time < ? AND d_id = ?";
            $query = $this->db->query($sql, array(date('Y-m-d'), $working_doctor->start_time, $working_doctor->end_time, $working_doctor->u_id));

            $count += intval($query->first_row()->total);

            if($count < $working_doctor->capacity){
                $available_doctors[] = $this->doctor_model->load_doctor($working_doctor);
            }

        }
        return $available_doctors;

    }

}
?>