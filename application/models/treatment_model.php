<?php
class Treatment_model extends CI_Model {

    var $treatment_id;
    var $e_id;
    var $story;
    var $diagnosis;
    var $treatment;
    var $doctor_id;

    public $examination;
    public $doctor;
    public $patient;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["treatment_id"] = $this->treatment_id;
        $data["e_id"] = $this->e_id;
        $data["story"] = $this->story;
        $data["diagnosis"] = $this->diagnosis;
        $data["treatment"] = $this->treatment;
        $data["doctor_id"] = $this->doctor_id;

        if( isset($this->examination) ){
            $data["examination"]  = $this->examination->as_array();
            $data["doctor"] = $this->doctor->as_array();
        }

        return $data;
    }

    function load_treatment($treatment){

        $new_treatment = new Treatment_model();

        $new_treatment->treatment_id = $treatment->treatment_id;
        $new_treatment->e_id = $treatment->e_id;
        $new_treatment->story = $treatment->story;
        $new_treatment->diagnosis = $treatment->diagnosis;
        $new_treatment->treatment = $treatment->treatment;
        $new_treatment->doctor_id = $treatment->doctor_id;

        return $new_treatment;
    }

    function get_treatment($_treatment_id){

        $this->load->model("examination_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);

        $sql = "SELECT * FROM Treatment NATURAL JOIN Examination , Doctor, User WHERE Doctor.d_id=User.user_id AND Treatment.doctor_id=Doctor.d_id AND Treatment.treatment_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($_treatment_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $this->treatment_id = $row->treatment_id;
            $this->e_id = $row->e_id;
            $this->story = $row->story;
            $this->diagnosis = $row->diagnosis;
            $this->treatment = $row->treatment;
            $this->doctor_id = $row->doctor_id;

            $this->examination = $this->examination_model->load_examination($row);
            $this->doctor = $this->doctor_model->load_doctor($row);
        }
        return $this;
    }

    function get_treatments_visit($visit_id){
        $this->load->model("examination_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);
        $this->load->model("department_model", "", TRUE);

        $sql = "SELECT * FROM Treatment NATURAL JOIN Examination, Doctor NATURAL JOIN Department, User WHERE Treatment.doctor_id=Doctor.d_id AND Doctor.d_id=User.user_id AND v_id = ? ORDER BY Examination.date DESC, Examination.time";
        $query = $this->db->query($sql, array($visit_id));

        $result_array = array();

        foreach($query->result() as $row){

            $treatment = $this->load_treatment($row);
            $treatment->examination = $this->examination_model->load_examination($row);
            $treatment->doctor = $this->doctor_model->load_doctor($row);
            $treatment->doctor->department = $this->department_model->load_department($row);

            $result_array[] = $treatment;
        }
        return $result_array;

    }

    function get_treatments_doctor($doctor_id, $s_date = NULL, $_date = NULL){
        $this->load->model("examination_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);
        $this->load->model("patient_model", "", TRUE);
        $this->load->model("visit_model", "", TRUE);

        if(isset($s_date) && isset($e_date) ){
            $s_date = date("Y-m-d", strtotime($s_date));
            $e_date = date("Y-m-d", strtotime($e_date));

            $sql = "SELECT * FROM Patient, Visit NATURAL JOIN Examination NATURAL JOIN Treatment, Doctor, User WHERE Patient.patient_id=Visit.p_id AND Examination.date BETWEEN ? AND ? AND Doctor.d_id=User.user_id AND Treatment.doctor_id=Doctor.d_id AND Treatment.doctor_id = ? ORDER BY Examination.date DESC, Examination.time";
            $query = $this->db->query($sql, array($s_date, $e_date, $doctor_id));
        }
        else{
            $sql = "SELECT * FROM Patient, Visit NATURAL JOIN Examination NATURAL JOIN Treatment, Doctor, User WHERE Patient.patient_id=Visit.p_id AND Doctor.d_id=User.user_id AND Treatment.doctor_id=Doctor.d_id AND Treatment.doctor_id = ? ORDER BY Examination.date DESC, Examination.time";
            $query = $this->db->query($sql, array($doctor_id));
        }  

        $result_array = array();

        foreach($query->result() as $row){

            $treatment = $this->load_treatment($row);
            $treatment->examination = $this->examination_model->load_examination($row);
            $treatment->doctor = $this->doctor_model->load_doctor($row);
            $treatment->patient = $this->patient_model->load_patient($row);
            $treatment->visit = $this->visit_model->load_visit($row);

            $result_array[] = $treatment;
        }
        return $result_array;

    }
    
    function insert_treatment($data)
    {
        $sql = "INSERT INTO Treatment (e_id, story, diagnosis, treatment, doctor_id)" .
                    " values " . 
                    "(?, ?, ?, ?, ?)";
        $this->db->query($sql, array($data["e_id"],
                                     $data["story"],
                                     $data["diagnosis"],
                                     $data["treatment"],
                                     $data["doctor_id"]));

        $sql = "SELECT LAST_INSERT_ID()";
        $query = $this->db->query($sql);

        return $query->result_array()[0]["LAST_INSERT_ID()"];
    }

}
?>