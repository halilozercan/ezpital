<?php
class Visit_model extends CI_Model {

    var $visit_id;
    var $enter_date;
    var $enter_time;
    var $end_date;
    var $end_time;
    var $reason;
    var $p_id;
    var $d_id;

    var $examination_count;

    public $patient;
    public $doctor;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["visit_id"] = $this->visit_id;
        $data["enter_time"] = $this->enter_time;
        $data["enter_date"] = $this->enter_date;
        $data["end_time"] = $this->end_time;
        $data["end_date"] = $this->end_date;
        $data["reason"] = $this->reason;
        $data["p_id"] = $this->p_id;
        $data["d_id"] = $this->d_id;

        if( isset($this->patient) ){
            $data["patient"]  = $this->patient->as_array();
            $data["doctor"] = $this->doctor->as_array();
        }

        return $data;
    }

    function load_visit($visit){

        $new_visit = new Visit_model();

        $new_visit->visit_id = $visit->visit_id;
        $new_visit->enter_time = $visit->enter_time;
        $new_visit->enter_date = $visit->enter_date;
        $new_visit->end_time = $visit->end_time;
        $new_visit->end_date = $visit->end_date;
        $new_visit->reason = $visit->reason;
        $new_visit->p_id = $visit->p_id;
        $new_visit->d_id = $visit->d_id;

        return $new_visit;
    }

    function get_visit($_visit_id){

        $this->load->model("patient_model", "", TRUE);
        $this->load->model("department_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);

        $sql = "SELECT * FROM Visit, Doctor NATURAL JOIN Department, User, Patient WHERE Visit.d_id=Doctor.d_id AND Doctor.d_id=User.user_id AND Visit.p_id=Patient.patient_id AND visit_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($_visit_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $this->visit_id = $row->visit_id;
            $this->enter_time = $row->enter_time;
            $this->enter_date = $row->enter_date;
            $this->end_time = $row->end_time;
            $this->end_date = $row->end_date;
            $this->reason = $row->reason;
            $this->p_id = $row->p_id;
            $this->d_id = $row->d_id;

            $this->patient = $this->patient_model->load_patient($row);
            $this->doctor = $this->doctor_model->load_doctor($row);
            $this->doctor->department = $this->department_model->load_department($row);
        }
        return $this;
    }
    
    function get_visits_patient($patient_id)
    {
        $this->load->model("patient_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);
        $this->load->model("department_model", "", TRUE);
        
        $sql = "SELECT * FROM Visit NATURAL JOIN Doctor NATURAL JOIN Department, User, Patient WHERE Patient.patient_id=Visit.p_id AND Doctor.d_id=User.user_id AND Visit.p_id = ? ORDER BY Visit.enter_date DESC, Visit.enter_time DESC"; 
        
        $query = $this->db->query($sql, array($patient_id));
        $result_array = array();

        foreach($query->result() as $row){
            $visit = $this->load_visit($row);
            $visit->doctor = $this->doctor_model->load_doctor($row);
            $visit->doctor->department = $this->department_model->load_department($row);
            $visit->patient = $this->patient_model->load_patient($row);
            array_push($result_array, $visit);
        }
        return $result_array;
    }

    function last_visit_patient($patient_id){

        $sql = "SELECT * FROM Visit WHERE p_id = ? ORDER BY visit_id DESC LIMIT 1";
        $query = $this->db->query($sql, array($patient_id));

        if($query->num_rows() > 0){
            $visit = $this->load_visit($query->first_row());
            return $visit;
        }
        else{
            $visit = new Visit_model();
            $visit->enter_date = "No Visit";
        }

        return $visit;

    }

    function get_visits_doctor($doctor_id, $s_date = NULL, $e_date = NULL)
    {
        $this->load->model("patient_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);
        $this->load->model("department_model", "", TRUE);
        
        if(isset($s_date) && isset($e_date) ){
            $s_date = date("Y-m-d", strtotime($s_date));
            $e_date = date("Y-m-d", strtotime($e_date));

            $sql = "SELECT * FROM Visit NATURAL JOIN Doctor NATURAL JOIN Department, User, Patient WHERE Patient.patient_id=Visit.p_id AND Doctor.d_id=User.user_id AND Visit.d_id = ? AND Visit.enter_date BETWEEN ? AND ? ORDER BY Visit.enter_date DESC, Visit.enter_time";     
            $query = $this->db->query($sql, array($doctor_id, $s_date, $e_date));
        }
        else{
            $sql = "SELECT * FROM Visit NATURAL JOIN Doctor NATURAL JOIN Department, User, Patient WHERE Patient.patient_id=Visit.p_id AND Doctor.d_id=User.user_id AND Visit.d_id = ? ORDER BY Visit.enter_date DESC, Visit.enter_time"; 
            $query = $this->db->query($sql, array($doctor_id));
        }        
        
        $result_array = array();

        foreach($query->result() as $row){

            $visit = $this->load_visit($row);
            $visit->doctor = $this->doctor_model->load_doctor($row);
            $visit->doctor->department = $this->department_model->load_department($row);
            $visit->patient = $this->patient_model->load_patient($row);

            $sql = "SELECT Count(*) as count FROM Examination WHERE v_id = ?"; 
            $query = $this->db->query($sql, array($visit->visit_id));

            $visit->examination_count = $query->first_row()->count;

            array_push($result_array, $visit);
        }
        return $result_array;
    }

    function is_there_open_visit($patient_id){

        $sql = "SELECT Count(*) as total FROM Visit WHERE p_id = ? AND end_date IS NULL";
        $query = $this->db->query($sql, array($patient_id));
        if($query->first_row()->total > 0)
            return TRUE;
        else
            return FALSE;

    }

    function get_visits_with_debts(){

        $this->load->model("patient_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);

        $sql = "SELECT * FROM Patient, Doctor, User, Visit NATURAL JOIN (SELECT debt_table.v_id as visit_id, debt_table.debt, paid_table.paid  FROM (SELECT Count(*) * Insurance_type.exam_price as debt, v_id FROM Examination, Visit, Patient, Insurance_type WHERE Examination.v_id = Visit.visit_id AND Visit.p_id = Patient.patient_id AND Patient.insurance_id = Insurance_type.insurance_id GROUP BY v_id) debt_table LEFT OUTER JOIN (SELECT Sum(Payment.amount) as paid, v_id FROM Payment GROUP BY v_id ) paid_table ON debt_table.v_id = paid_table.v_id WHERE debt > paid OR paid is NULL) debt_visits WHERE end_date IS NOT NULL AND Patient.patient_id = Visit.p_id AND Doctor.d_id = Visit.d_id AND Doctor.d_id = User.user_id";
        $query = $this->db->query($sql);

        $result_array = array();

        foreach($query->result() as $row){
            $result = $this->load_visit($row);
            $result->patient = $this->patient_model->load_patient($row);
            $result->doctor = $this->doctor_model->load_doctor($row);
            if(isset($row->paid))
                $result->paid = $row->paid;
            else
                $result->paid = 0;
            $result->debt = $row->debt;

            $result_array[] = $result;
        }

        return $result_array;
    }

    function search_visits($data){

        $this->load->model("patient_model", "", TRUE);
        $this->load->model("doctor_model", "", TRUE);
        $this->load->model("department_model", "", TRUE);

        $p_name = $data['patient_name'];
        $p_surname = $data['patient_surname'];

        $sql = "SELECT * FROM Visit NATURAL JOIN Doctor NATURAL JOIN Department, User, Patient WHERE Doctor.d_id = User.user_id AND Visit.p_id = Patient.patient_id AND Patient.patient_name LIKE '%$p_name%' AND Patient.patient_surname LIKE '%$p_surname%' AND Visit.enter_date BETWEEN ? AND ? ";
        $query = $this->db->query($sql, array($data['start_date'],
                                              $data['end_date']));

        $result_array = array();

        foreach($query->result() as $row){
            $result = $this->load_visit($row);
            $result->patient = $this->patient_model->load_patient($row);
            $result->doctor = $this->doctor_model->load_doctor($row);
            $result->doctor->department = $this->department_model->load_department($row);

            $result_array[] = $result;
        }

        return $result_array;
    }

    function insert_visit($data)
    {
        $date = date("Y-m-d");
        $time = date("h:i:s");
        $sql = "INSERT INTO Visit (enter_date, enter_time, reason, p_id, d_id)" .
                    " values " . 
                    "(?, ?, ?, ?, ?)";
        $this->db->query($sql, array($date,
                                     $time,
                                     $data["reason"],
                                     $data["p_id"],
                                     $data["d_id"]));

        $sql = "SELECT LAST_INSERT_ID();";
        $query = $this->db->query ( $sql );
        
        return $query->result_array ()[0]["LAST_INSERT_ID()"];
    }

    function done_visit($visit_id){
        $date = date("Y-m-d");
        $time = date("h:i:s");
        $sql = "UPDATE Visit SET end_date = ?, end_time = ? WHERE visit_id = ?";
        $this->db->query($sql, array($date,
                                     $time,
                                     $visit_id));

    }

}
?>