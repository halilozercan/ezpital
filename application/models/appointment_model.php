<?php
class Appointment_model extends CI_Model {
    var $appointment_id;
    var $national_id;
    var $a_name;
    var $a_surname;
    var $date;
    var $time;
    var $d_id;

    public $doctor;
    public $patient;

    function __construct() {
        // Call the Model constructor
        parent::__construct ();
    }

    function load_appointment($appointment) {
        $new_appointment = new Appointment_model ();
        
        $new_appointment->appointment_id = $appointment->appointment_id;
        $new_appointment->national_id = $appointment->national_id;
        $new_appointment->a_name = $appointment->a_name;
        $new_appointment->a_surname = $appointment->a_surname;
        $new_appointment->date = $appointment->date;
        $new_appointment->time = $appointment->time;
        $new_appointment->d_id = $appointment->d_id;
        
        return $new_appointment;
    }

    function is_appointment_available($department_id, $date, $time){
        $week_day = date('N', strtotime($date));

        $this->load->model('doctor_model', '', TRUE);

        $available_doctors = $this->doctor_model->get_available_doctors($department_id, $week_day, $time);

        if(empty($available_doctors)){
            return FALSE;
        }
        else{
            return $available_doctors[ rand( 0, count($available_doctors)-1 ) ];
        }

    }

    function any_other_appointment($department_id, $date, $national_id){

        $sql = "SELECT Count(*) as total FROM Appointment, Doctor NATURAL JOIN Department WHERE Department.department_id = ? AND Appointment.date = ? AND Appointment.national_id = ? ";
        $query = $this->db->query ( $sql, array($department_id, $date, $national_id) );

        return $query->first_row()->total;
    }

    function find_appointments($name, $surname, $national_id){

        $this->load->model('doctor_model', '', TRUE);
        $this->load->model('patient_model', '', TRUE);
        $this->load->model('doctor_model', '', TRUE);

        $sql = "SELECT * FROM Appointment NATURAL JOIN Doctor, User WHERE Doctor.d_id = User.user_id AND Appointment.a_name LIKE '%$name%' AND Appointment.a_surname LIKE '%$surname%' AND Appointment.national_id LIKE '%$national_id%' ";
        $query = $this->db->query ( $sql, array($name, $surname, $national_id) );

        $result_array = array();

        foreach($query->result() as $row){
            $appointment = $this->load_appointment($row);
            $appointment->doctor = $this->doctor_model->load_doctor($row);
            $appointment->patient = $this->patient_model->get_patient_national_id($appointment->national_id);

            $result_array[] = $appointment;
        }

        return $result_array;

    }

    function done_appointment($appointment_id){
        $sql = "DELETE FROM Appointment WHERE appointment_id = ?";
        $query = $this->db->query ( $sql, $appointment_id );
    }

    function insert_appointment($data) {
        
        $sql = "INSERT INTO Appointment (a_name, a_surname, national_id, date, time, d_id) VALUES(?,?,?,?,?,?);";
        $query = $this->db->query ( $sql, $data );
        
        $sql = "SELECT LAST_INSERT_ID();";
        $query = $this->db->query ( $sql );
        
        return $query->result_array ()[0]["LAST_INSERT_ID()"];
    }
}
?>