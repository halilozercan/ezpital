<?php
class Examination_model extends CI_Model {
	var $e_id;
	var $v_id;
	var $date;
	var $time;
	public $visit;
	function __construct() {
		// Call the Model constructor
		parent::__construct ();
	}
	function as_array() {
		$data ["e_id"] = $this->e_id;
		$data ["v_id"] = $this->v_id;
		$data ["date"] = $this->date;
		$data ["time"] = $this->time;
		
		if (isset ( $this->visit )) {
			$data ["visit"] = $this->visit->as_array ();
		}
		
		return $data;
	}
	function get_examination_visit($_visit_id) {
		$sql = "SELECT * FROM Examination WHERE v_id = ?";
		$query = $this->db->query ( $sql, array (
				$_visit_id 
		) );
		return $query->result_array();
	}
	function load_examination($examination) {
		$new_examination = new Examination_model ();
		
		$new_examination->e_id = $examination->e_id;
		$new_examination->v_id = $examination->v_id;
		$new_examination->date = $examination->date;
		$new_examination->time = $examination->time;
		
		return $new_examination;
	}
	function insert_examination($v_id) {
		$date = date ( "Y-m-d" );
		$time = date ( "h:i:s" );
		
		$sql = "INSERT INTO Examination (v_id,date,time) VALUES(?,?,?);";
		$query = $this->db->query ( $sql, array (
				$v_id,
				$date,
				$time 
		) );
		
		$sql = "SELECT LAST_INSERT_ID();";
		$query = $this->db->query ( $sql );
		
		return $query->result_array ()[0]["LAST_INSERT_ID()"];
	}
}
?>