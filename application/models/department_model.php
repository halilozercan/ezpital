<?php
class Department_model extends CI_Model {

    var $department_id;
    var $department_name;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["department_id"] = $this->department_id;
        $data["name"] = $this->name;

        return $data;
    }

    function load_department($department){
        $this->department_id = $department->department_id;
        $this->department_name = $department->department_name;

        return $this;
    }

    function get_all_departments(){

        $sql = "SELECT Department.department_id, department_name, count(ActiveUser.department_id) as no
FROM (SELECT department_id FROM Doctor, User WHERE Doctor.d_id = User.user_id AND User.active=1) AS ActiveUser RIGHT OUTER JOIN Department USING (department_id) GROUP BY department_id;";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function get_department_with_name( $name) {

        $sql = "SELECT * FROM Department WHERE department_name='$name' LIMIT 1";
        $query = $this->db->query( $sql);

        if( $query->num_rows > 0) {

            return $query->result_array();
        }

        return false;
    }

    function insert_department( $name) {

        $sql = "INSERT INTO Department(department_name) VALUES( ?)";
        $query = $this->db->query( $sql, array( $name));
    }

    function delete_department( $departmentId) {

        $sql = "DELETE FROM Department WHERE department_id=?";
        $query = $this->db->query( $sql, array( $departmentId));
    }

    function get_department($_department_id){

        $sql = "SELECT * FROM Department WHERE department_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($_department_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $this->department_id = $row->department_id;
            $this->name = $row->department_name;
        }

        return $this;
    }

}
?>