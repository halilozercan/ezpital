<?php
class Patient_model extends CI_Model {

    var $patient_id;
    var $national_id;
    var $patient_name;
    var $patient_surname;
    var $birth_date;
    var $blood_type;
    var $insurance_id;
    var $insurance_number;
    var $telephone;
    var $address;

    var $insurance_name;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["patient_id"] = $this->patient_id;
        $data["national_id"] = $this->national_id;
        $data["patient_name"] = $this->patient_name;
        $data["patient_surname"] = $this->patient_surname;
        $data["birth_date"] = $this->birth_date;
        $data["blood_type"] = $this->blood_type;
        $data["insurance_id"] = $this->insurance_id;
        $data["insurance_number"] = $this->insurance_number;
        $data["telephone"] = $this->telephone;
        $data["address"] = $this->address;

        if( isset($this->insurance_name) ){
            $data["insurance_name"] = $this->insurance_name;
        }

        return $data;
    }

    function load_patient($patient){

        $new_patient = new Patient_model();

        $new_patient->patient_id = $patient->patient_id;
        $new_patient->national_id = $patient->national_id;
        $new_patient->patient_name = $patient->patient_name;
        $new_patient->patient_surname = $patient->patient_surname;
        $new_patient->birth_date = $patient->birth_date;
        $new_patient->blood_type = $patient->blood_type;
        $new_patient->insurance_id = $patient->insurance_id;
        $new_patient->insurance_number = $patient->insurance_number;
        $new_patient->telephone = $patient->telephone;
        $new_patient->address = $patient->address;

        return $new_patient;
    }

    function get_patient($_patient_id){

        $sql = "SELECT * FROM Patient, Insurance_type WHERE Patient.insurance_id=Insurance_type.insurance_id AND patient_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($_patient_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $this->patient_id = $row->patient_id;
            $this->national_id = $row->national_id;
            $this->patient_name = $row->patient_name;
            $this->patient_surname = $row->patient_surname;
            $this->birth_date = $row->birth_date;
            $this->blood_type = $row->blood_type;
            $this->insurance_id = $row->insurance_id;
            $this->insurance_number = $row->insurance_number;
            $this->telephone = $row->telephone;
            $this->address = $row->address;

            $this->insurance_name = $row->insurance_name;
        }
        return $this;
    }

    function get_patient_national_id($national_id){

        $sql = "SELECT * FROM Patient, Insurance_type WHERE Patient.insurance_id=Insurance_type.insurance_id AND national_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($national_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $patient = $this->load_patient($row);
            $patient->insurance_name = $row->insurance_name;
            return $patient;
        }
        else{
            return FALSE;
        }
        
    }

    function get_patient_with_visits($_patient_id){

        $sql = "SELECT * FROM Patient, Visit WHERE patient_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($_patient_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $this->patient_id = $row->patient_id;
            $this->national_id = $row->national_id;
            $this->patient_name = $row->patient_name;
            $this->patient_surname = $row->patient_surname;
            $this->birth_date = $row->birth_date;
            $this->blood_type = $row->blood_type;
            $this->insurance_id = $row->insurance_id;
            $this->insurance_number = $row->insurance_number;
            $this->telephone = $row->telephone;
            $this->address = $row->address;
        }
        return $this;
    }
    
    function find_patients($name, $surname, $inpatient = '0')
    {
        $this->load->model('visit_model', '', TRUE);

        if($inpatient == '0'){
            $sql = "SELECT * FROM Patient WHERE patient_name LIKE '%$name%' AND patient_surname LIKE '%$surname%'"; 
        }
        else if($inpatient == '1'){
            $sql = "SELECT * FROM Patient WHERE patient_name LIKE '%$name%' AND patient_surname LIKE '%$surname%' AND patient_id IN (SELECT patient_id FROM Inpatient)";        
        }
        else if($inpatient == '2'){
            $sql = "SELECT * FROM Patient WHERE patient_name LIKE '%$name%' AND patient_surname LIKE '%$surname%' AND patient_id NOT IN (SELECT patient_id FROM Inpatient)";        
        }

        $query = $this->db->query($sql);

        $patients = array();

        foreach($query->result() as $row){

            $patient = $this->load_patient($row);
            $patient->last_visit = $this->visit_model->last_visit_patient($patient->patient_id);

            $patients[] = $patient;
        }

        return $patients;
    }

    function report($patient_id){

        $sql = "SELECT * FROM (SELECT SUM(debt) as total_debt FROM (SELECT Count(*) * Insurance_type.exam_price as debt, v_id, Visit.p_id FROM Examination, Visit, Patient, Insurance_type WHERE Examination.v_id = Visit.visit_id AND Visit.p_id = Patient.patient_id AND Patient.insurance_id = Insurance_type.insurance_id GROUP BY v_id) debt_table) total_debt_table, (SELECT SUM(amount) as total_payment FROM Payment) total_payment_table";
        $query = $this->db->query($sql, array($patient_id));

        $result_array = array();

        foreach($query->result() as $row){

            $visit = $this->load_visit($row);
            $visit->debt = $row->debt;

            $result_array[] = $visit;
        }

        return $result_array;
    }

    function insert_patient($data)
    {
        $sql = "INSERT INTO Patient (national_id, patient_name, patient_surname, birth_date, blood_type, insurance_id, insurance_number, telephone, address)" .
                    " values " . 
                    "(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $this->db->query($sql, array($data["nationalid"],
                                     $data["patient_name"],
                                     $data["patient_surname"],
                                     $data["birthdate"],
                                     $data["bloodtype"],
                                     $data["insurancetype"],
                                     $data["insurancenumber"],
                                     $data["phone"],
                                     $data["address"]));
    }

    function update_patient($data)
    {
        $this->title   = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date    = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

}
?>