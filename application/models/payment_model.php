<?php
class Payment_model extends CI_Model {
	var $payment_id;
	var $v_id;
	var $amount;
	var $currency;
	var $date;
	var $time;

	public $visit;

	function __construct() {
		parent::__construct ();
	}

	function load_payment($payment){

		$new_payment = new Payment_model();

		$new_payment->payment_id = $payment->payment_id;
		$new_payment->v_id = $payment->v_id;
		$new_payment->amount = $payment->amount;
		$new_payment->currency = $payment->currency;
		$new_payment->date = $payment->date;
		$new_payment->time = $payment->time;

		return $new_payment;
	}

	function get_payment($_payment_id) {

		$this->load->model('visit_model', '', TRUE);

		$sql = "SELECT * FROM Payment, Visit WHERE payment.v_id=Visit.visit_id AND Payment.payment_id = ? LIMIT 1";
		$query = $this->db->query ( $sql, array (
				$_payment_id
		) );
		
		if ($query->num_rows () > 0) {
			$row = $query->first_row ();
			
			$payment = $this->load_payment($row);
			$payment->visit = $this->visit_model->load_visit($row);
			return $payment;
		}
		else{
			return FALSE;
		}
		
		
	}

	function insert_payment($data)
    {
        
        $date = date("Y-m-d");
        $time = date("H:i:s");
        $sql = "INSERT INTO Payment (v_id, amount, currency, date, time)" .
                    " values " . 
                    "(?, ?, ?, ?, ?)";
        $this->db->query($sql, array($data["v_id"],
                                     $data["amount"],
                                     $data["currency"],
                                     $date,
                                     $time));

        $sql = "SELECT LAST_INSERT_ID();";
        $query = $this->db->query($sql);

        return $query->result_array()[0]["LAST_INSERT_ID()"];
    }
}
?>