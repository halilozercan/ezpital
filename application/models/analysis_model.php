<?php
class Analysis_model extends CI_Model {

    var $analysis_id;
    var $e_id;
    var $analysis_name;
    var $report;
    var $n_id;

    public $examination;
    public $nurse;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["analysis_id"] = $this->analysis_id;
        $data["e_id"] = $this->e_id;
        $data["analysis_name"] = $this->analysis_name;
        $data["report"] = $this->report;
        $data["n_id"] = $this->n_id;

        if( isset($this->examination) ){
            $data["examination"]  = $this->examination->as_array();
            $data["nurse"] = $this->nurse->as_array();
        }

        return $data;
    }

    function load_analysis($analysis){

        $new_analysis = new Analysis_model();

        $new_analysis->analysis_id = $analysis->analysis_id;
        $new_analysis->e_id = $analysis->e_id;
        $new_analysis->analysis_name = $analysis->analysis_name;
        $new_analysis->report = $analysis->report;
        $new_analysis->n_id = $analysis->n_id;

        return $new_analysis;
    }

    function get_analysis($_analysis_id){

        $this->load->model("examination_model", "", TRUE);
        $this->load->model("nurse_model", "", TRUE);

        $sql = "SELECT * FROM Analysis NATURAL JOIN Examination NATURAL JOIN Nurse, User WHERE Nurse.n_id=User.user_id AND analysis_id = ? LIMIT 1";
        $query = $this->db->query($sql, array($_analysis_id));

        if($query->num_rows() > 0){
            $row = $query->first_row();

            $this->analysis_id = $row->analysis_id;
            $this->e_id = $row->e_id;
            $this->analysis_name = $row->analysis_name;
            $this->report = $row->report;
            $this->n_id = $row->n_id;

            $this->examination = $this->examination_model->load_examination($row);
            $this->nurse = $this->nurse_model->load_nurse($row);

            if($this->report == NULL){
                $this->report = "Not completed yet!";
            }
        }
        return $this;
    }

    function get_analysises_visit($visit_id){
        $this->load->model("examination_model", "", TRUE);
        $this->load->model("nurse_model", "", TRUE);

        $sql = "SELECT * FROM Analysis NATURAL JOIN Examination NATURAL JOIN Nurse, User WHERE Nurse.n_id=User.user_id AND v_id = ?";
        $query = $this->db->query($sql, array($visit_id));

        $result_array = array();

        foreach($query->result() as $row){
            $analysis = $this->load_analysis($row);
            $analysis->examination = $this->examination_model->load_examination($row);
            $analysis->nurse = $this->nurse_model->load_nurse($row);
            array_push($result_array, $analysis);
        }
        return $result_array;

    }

    function get_all_analyses($n_id){

        $this->load->model('nurse_model', '', TRUE);
        $this->load->model('visit_model', '', TRUE);
        $this->load->model('patient_model', '', TRUE);
        $this->load->model('examination_model', '', TRUE);

        $sql = "SELECT * FROM Analysis NATURAL JOIN Examination NATURAL JOIN Nurse, User, Visit, Patient WHERE Patient.patient_id = Visit.p_id AND Visit.visit_id = Examination.v_id AND Nurse.n_id=User.user_id AND Analysis.n_id = ?";
        $query = $this->db->query($sql, array($n_id));

        $result_array = array();

        foreach($query->result() as $row){

            $analysis = $this->load_analysis($row);
            $analysis->nurse = $this->nurse_model->load_nurse($row);
            $analysis->examination = $this->examination_model->load_examination($row);
            $analysis->examination->visit = $this->visit_model->load_visit($row);
            $analysis->examination->visit->patient = $this->patient_model->load_patient($row);

            $result_array[] = $analysis;
        }

        return $result_array;
    }

    function get_waiting_analyses($n_id){

        $this->load->model('nurse_model', '', TRUE);
        $this->load->model('visit_model', '', TRUE);
        $this->load->model('patient_model', '', TRUE);
        $this->load->model('examination_model', '', TRUE);

        $sql = "SELECT * FROM Analysis NATURAL JOIN Examination NATURAL JOIN Nurse, User, Visit, Patient WHERE Patient.patient_id = Visit.p_id AND Analysis.report IS NULL AND Visit.visit_id = Examination.v_id AND Nurse.n_id=User.user_id AND Analysis.n_id = ?";
        $query = $this->db->query($sql, array($n_id));

        $result_array = array();

        foreach($query->result() as $row){

            $analysis = $this->load_analysis($row);
            $analysis->nurse = $this->nurse_model->load_nurse($row);
            $analysis->examination = $this->examination_model->load_examination($row);
            $analysis->examination->visit = $this->visit_model->load_visit($row);
            $analysis->examination->visit->patient = $this->patient_model->load_patient($row);

            $result_array[] = $analysis;
        }

        return $result_array;
    }
    
    function get_analysises_visit_from_nurse($visit_id, $nurse_id){
    	$this->load->model("examination_model", "", TRUE);
    	$this->load->model("nurse_model", "", TRUE);
    
    	$sql = "SELECT * FROM Analysis NATURAL JOIN Examination NATURAL JOIN Nurse, User WHERE Nurse.n_id=User.user_id AND v_id = ? AND Analysis.n_id = ?";
    	$query = $this->db->query($sql, array($visit_id, $nurse_id));
    
    	$result_array = array();
    
    	foreach($query->result() as $row){
    		$analysis = $this->load_analysis($row);
    		$analysis->examination = $this->examination_model->load_examination($row);
    		$analysis->nurse = $this->nurse_model->load_nurse($row);
    		array_push($result_array, $analysis);
    	}
    	return $result_array;
    
    }
    
    function insert_analysis_doctor($data)
    {
        $today = date("Y-m-d");
        $sql = "SELECT * FROM " . 
                    "((SELECT * FROM Nurse NATURAL JOIN " . 
                        "(SELECT Count(n_id) as count, n_id FROM Analysis NATURAL JOIN Examination WHERE Examination.date = ?  GROUP BY n_id) nurses)" . 
                        " UNION " . 
                        "(SELECT n_id, '0' as count FROM Nurse)) " . 
                    "all_result GROUP BY n_id;";

        $query = $this->db->query($sql, array($today));

        $minimum = 10000;
        $minimum_n_id = 0;

        foreach($query->result() as $row){
            if($row->count < $minimum){
                $minimum = $row->count;
                $minimum_n_id = $row->n_id;
            }
        }


        $date = date("Y-m-d");
        $time = date("H:i:s");
        $sql = "INSERT INTO Analysis (e_id, analysis_name, n_id)" .
                    " values " . 
                    "(?, ?, ?)";
        $this->db->query($sql, array($data["e_id"],
                                     $data["analysis_name"],
                                     $minimum_n_id));

        $sql = "SELECT LAST_INSERT_ID();";
        $query = $this->db->query($sql);

        return $query->result_array()[0]["LAST_INSERT_ID()"];
    }
    
    function insert_analysis_nurse($data) {
    	$today = date("Y-m-d");
    	$sql = "UPDATE Analysis SET report=? WHERE analysis_id=?";
    	$this->db->query($sql, array($data["report"],
    			$data["analysisID"]));
    }
}
?>