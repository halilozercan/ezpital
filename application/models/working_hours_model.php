<?php
class Working_hours_model extends CI_Model {

    var $id;
    var $u_id;
    var $day;
    var $start_time;
    var $end_time;
    var $capacity;

    public $user;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["doctor_id"] = $this->doctor_id;
        $data["email"] = $this->email;
        $data["name"] = $this->enter_date;
        $data["surname"] = $this->end_time;
        $data["r_id"] = $this->end_date;
        $data["picture_url"] = $this->reason;
        $data["title"] = $this->p_id;
        $data["department_id"] = $this->d_id;

        if( isset($this->room) ){
            //$data["room"]  = $this->room->as_array();
            $data["department"] = $this->department->as_array();
        }

        return $data;
    }

    function load_working_hour($working_hour){

        $new_working_hour = new Working_hours_model();

        $new_working_hour->id = $working_hour->id;
        $new_working_hour->u_id = $working_hour->u_id;
        $new_working_hour->day = $working_hour->day;
        $new_working_hour->start_time = $working_hour->start_time;
        $new_working_hour->end_time = $working_hour->end_time;
        $new_working_hour->capacity = $working_hour->capacity;

        return $new_working_hour;
    }

    function get_weekly_schedule($_u_id){

        $this->load->model('user_model', '', TRUE);

        $sql = "SELECT * FROM User, Works_at WHERE User.user_id=Works_at.u_id AND Works_at.u_id = ?";
        $query = $this->db->query($sql, array($_u_id));

        $weekly_schedule = array();

        foreach($query->result() as $row){

            $working_hour = $this->load_working_hour($row);
            $working_hour->user = $this->user_model->load_user($row);

            array_push($weekly_schedule, $working_hour);

        }

        return $weekly_schedule;
    }

    function is_overlapping($u_id, $day, $start_time, $end_time){

        $sql = "SELECT * FROM Works_at WHERE day = ? AND ((start_time < ? AND end_time > ?) OR (start_time < ? AND end_time > ?)) AND u_id = ?";
        $query = $this->db->query($sql, array($day, $start_time, $start_time, $end_time, $end_time, $u_id));

        if($query->num_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    function insert_working_hour($data){

        $sql = "INSERT INTO Works_at (u_id, day, start_time, end_time, capacity) VALUES (?, ?, ?, ?, ?)";
        $this->db->query($sql, array($data['u_id'], $data['day'], $data['start_time'], $data['end_time'], $data['capacity']));
    }

    function cancel_working_hour($id){

        $sql = "DELETE FROM Works_at WHERE id = ? AND u_id = ?";
        $this->db->query($sql, array($id, $this->userData['id']));
    }

    function get_available_doctors($department_id){

        $this->load->model("visit_model", "", TRUE);

        $week_day = intval(date("N"));
        $time = date("h:i:s");

        $sql = "SELECT * FROM Works_at WHERE day = ? AND start_time < ? AND end_time > ? u_id IN (SELECT d_id as u_id FROM Doctor WHERE Doctor.department_id = ? )";
        $query = $this->db->query($sql, array($week_day, $time, $time, $department_id));

        foreach($query->result() as $working_doctor){
            var_dump($working_doctor);
        }

        $visits = $this->visit_model->get_remaining_visits();

    }

}
?>