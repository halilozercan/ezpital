<?php
class User_model extends CI_Model {

    var $user_id;
    var $username;
    var $email;
    var $name;
    var $surname;
    var $reg_date;
    var $picture_url;
    var $type;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function as_array(){
        $data["user_id"] = $this->user_id;
        $data["username"] = $this->username;
        $data["email"] = $this->email;
        $data["name"] = $this->name;
        $data["surname"] = $this->surname;
        $data["reg_date"] = $this->reg_date;
        $data["picture_url"] = $this->picture_url;
        $data["type"] = $this->type;

        return $data;
    }

    function load_user($user){
        $new_user = new User_model();

        $new_user->user_id = $user->user_id;
        $new_user->username = $user->username;
        $new_user->email = $user->email;
        $new_user->name = $user->name;
        $new_user->surname = $user->surname;
        $new_user->reg_date = $user->reg_date;
        $new_user->picture_url = $user->picture_url;
        $new_user->type = $user->type;

        return $new_user;
    }

    function get_user( $user_id) {

    	$sql = "SELECT * FROM User WHERE user_id = ? LIMIT 1";
    	$query = $this->db->query( $sql, array( $user_id));

    	if( $query->num_rows() > 0) {

    		$row = $query->first_row();

    		$this->user_id = $row->user_id;
    		$this->username = $row->username;
            $this->email = $row->email;
            $this->name = $row->name;
            $this->surname = $row->surname;
            $this->reg_date = $row->reg_date;
            $this->picture_url = $row->picture_url;
            $this->type = $row->type;

            return $this;
    	}

    	return false;
    }

    function get_users( $keyword) {

        $sql = "SELECT * FROM User WHERE LOWER(username) LIKE LOWER('%$keyword%') OR LOWER(CONCAT(name,' ',surname)) LIKE LOWER('%$keyword%')";
        $query = $this->db->query( $sql);

        return $query->result_array();
    }

    function is_username_exist( $username) {

        $sql = "SELECT * FROM User WHERE username=? LIMIT 1";
        $query = $this->db->query( $sql, array( $username));

        if( $query->num_rows() > 0)
            return true;

        return false;
    }

    function get_all_users( $id) {

    	$sql = "SELECT * FROM User WHERE active=1 AND user_id<>?";
    	$query = $this->db->query( $sql, array( $id));

    	return $query->result_array();
    }

    function delete_user( $userId) {

        $sql = "UPDATE  User SET active=0 WHERE user_id=?";

        $query = $this->db->query( $sql, array( $userId));
    }

    function get_doctor( $doctorId) {

        $sql = "SELECT * FROM Doctor WHERE d_id=? LIMIT 1";
        $query = $this->db->query( $sql, array( intval( $doctorId)));

        $result = $query->result_array();
        return $result[0];
    }

    function insert_user( $data) {

        $sql = "INSERT INTO User( username, password, email, reg_date, name, surname, type) values " .
               "(?,?,?,CURDATE(),?,?,?)";

        $type = $data['type'];

        $this->db->query($sql, array($data["username"],
                             md5( $data["password"]),
                             $data["email"],
                             $data["name"],
                             $data["surname"],
                             $data["type"]));

        $sql = "SELECT LAST_INSERT_ID();";
        $query = $this->db->query($sql);
        
        $userId = $query->result_array()[0]["LAST_INSERT_ID()"];

        switch( $type) {

        case( 'm'):

            $sql = "INSERT INTO Manager(m_id) VALUES ($userId)";
            break;

        case( 's'):

            $sql = "INSERT INTO Secretary(s_id) VALUES ($userId)";
            break;

        case( 'n'):

            $sql = "INSERT INTO Nurse(n_id) VALUES ($userId)";
            break;

        case( 'd'):

            $title = $data['title'];
            $department_id =  $data['department'];
            $r_id = $data['r_id'];
            $sql = "INSERT INTO Doctor(d_id, title, department_id, r_id) VALUES ($userId, '$title', '$department_id', $r_id)";
            break;
        }

        $query = $this->db->query( $sql);
    }
}
?>
