<?php
class Nurse_model extends CI_Model {
	var $nurse_id;
	var $email;
	var $name;
	var $surname;
	var $picture_url;

	function __construct() {
		parent::__construct ();
	}

	function as_array() {
		$data ["n_id"] = $this->nurse_id;
		$data ["email"] = $this->email;
		$data ["name"] = $this->name;
		$data ["surname"] = $this->surname;
		$data ["picture_url"] = $this->picture_url;
		
		return $data;
	}

	function load_nurse($nurse){

		$new_nurse = new Nurse_model();

		$new_nurse->nurse_id = $nurse->n_id;
		$new_nurse->email = $nurse->email;
		$new_nurse->name = $nurse->name;
		$new_nurse->surname = $nurse->surname;
		$new_nurse->picture_url = $nurse->picture_url;

		return $new_nurse;
	}

	function get_nurse($_nurse_id) {

		$this->load->model("room_model", "", TRUE);

		$sql = "SELECT * FROM Nurse, User WHERE Nurse.n_id=User.user_id AND Nurse.n_id = ? LIMIT 1";
		$query = $this->db->query ( $sql, array (
				$_nurse_id 
		) );
		
		if ($query->num_rows () > 0) {
			$row = $query->first_row ();
			
			$this->nurse_id = $row->n_id;
			$this->email = $row->email;
			$this->name = $row->name;
			$this->surname = $row->surname;
			$this->picture_url = $row->picture_url;
		}
		
		return $this;
	}

	function get_today_analyses_count(){

		$sql = "SELECT Count(*) as total FROM Examination NATURAL JOIN Analysis NATURAL JOIN Nurse, User WHERE Examination.date = ? AND Nurse.n_id=User.user_id AND Nurse.n_id = ? AND Analysis.report IS NOT NULL";
		$query = $this->db->query ( $sql, array (
				date("Y-m-d"),
				$this->nurse_id
		) );

		return $query->first_row()->total;
	}

	function get_waiting_analyses_count(){

		$sql = "SELECT Count(*) as total FROM Examination NATURAL JOIN Analysis NATURAL JOIN Nurse, User WHERE Examination.date = ? AND Nurse.n_id=User.user_id AND Nurse.n_id = ? AND Analysis.report IS NULL";
		$query = $this->db->query ( $sql, array (
				date("Y-m-d"),
				$this->nurse_id
		) );

		return $query->first_row()->total;
	}
}
?>