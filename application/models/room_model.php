<?php
class Room_model extends CI_Model {
	var $room_id;
	var $location;
	var $number;
	
	function __construct() {
		parent::__construct ();
	}

	function as_array() {
		$data ["room_id"] = $this->room_id;
		$data ["location"] = $this->location;
		$data ["number"] = $this->number;
	
		return $data;
	}

	function load_room($room){

		$this->room_id = $room->room_id;
		$this->location = $room->location;
		$this->number = $room->number;
	}

	function get_room($_room_id) {
		$sql = "SELECT * FROM Room WHERE room_id = ? LIMIT 1";
		$query = $this->db->query ( $sql, array (
				$_room_id
		) );
	
		if ($query->num_rows () > 0) {
			$row = $query->first_row ();
				
			$this->room_id = $row->room_id;
			$this->location = $row->location;
			$this->number = $row->number;
		}
	
		return $this;
	}

	function get_sickroom( $room_id) {

		$result = $this -> get_room( $room_id) -> as_array();

		$sql = "SELECT capacity FROM Sickroom WHERE r_id = ? LIMIT 1";
		$query = $this->db->query ( $sql, array (
				$room_id
		) );
	
		if ($query->num_rows () > 0) {
			$row = $query->first_row ();
			
			$result['capacity'] = $row->capacity;
		}

		return $result;
	}

	function is_office_empty( $officeId) {

		$sql = "SELECT * FROM Office NATURAL JOIN Doctor WHERE Office.r_id=?";
		$query = $this->db->query ( $sql, array (
				$officeId
		) );

		if ($query->num_rows () > 0) {

			return false;
		}

		return true;
	}

	function is_sickroom_empty( $sickroomId) {

		$sql = "SELECT * FROM Sickroom, Inpatient WHERE Sickroom.r_id=Inpatient.room_id AND Inpatient.end_time is NULL";
		$query = $this->db->query ( $sql, array (
				$sickroomId
		) );

		if ($query->num_rows () > 0) {

			return false;
		}

		return true;
	}

	function delete_room( $officeId) {

		$sql = "DELETE FROM Office WHERE Office.r_id=?";
		$query = $this->db->query ( $sql, array (
				$officeId
		) );
	}

	function delete_sickroom( $sickroomId) {

		$sql = "DELETE FROM Sickroom WHERE Sickroom.r_id=?";
		$query = $this->db->query ( $sql, array (
				$sickroomId
		) );
	}

	function update_sickroom( $sickroomId, $data) {

		$sickroomId = intval( $sickroomId);
		$sickroom = $this->get_room_with_number( $data[0]);
		if( $sickroom != false)
			if( $sickroom['room_id'] != $sickroomId)
				return false;


		$sql = "UPDATE Room SET number=?, location=? WHERE room_id=?";
		$query = $this->db->query ( $sql, array (
										  $data[0],
										  $data[1],
										  $sickroomId
		) );

		$sql = "UPDATE Sickroom SET capacity=? WHERE r_id=?";
		$query = $this->db->query ( $sql, array (
										  $data[2],
										  $sickroomId
		) );
		return true;
	}

	function update_office( $officeId, $data) {

		$office = $this->get_room_with_number( $data[0]);
		if( $office != false)
			if( $office['room_id'] != $officeId)
				return false;


		$sql = "UPDATE Room SET number=?, location=? WHERE room_id=?";
		$query = $this->db->query ( $sql, array (
										  $data[0],
										  $data[1],
										  $officeId
		) );
		return true;
	}

	function get_room_with_number($roomNumber) {

		$sql = "SELECT * FROM Room WHERE number = ? LIMIT 1";
		$query = $this->db->query ( $sql, array (
				$roomNumber
		) );

		if ($query->num_rows () > 0) {
			$row = $query->first_row ();
				
			$this->room_id = $row->room_id;
			$this->location = $row->location;
			$this->number = $row->number;
			return $this->as_array();
		}
	
		return false;
	}

	function get_all_offices() {

		$sql = "SELECT * FROM Room,Office WHERE Room.room_id = Office.r_id";
		$query = $this->db->query( $sql);

		return $query->result_array();
	}

	function get_all_sickrooms() {

		$sql = "SELECT * FROM Room,Sickroom WHERE Room.room_id = Sickroom.r_id";
		$query = $this->db->query( $sql);

		return $query->result_array();
	}

	function insert_room( $data, $isOffice) {

		$sql = "INSERT INTO Room( number, location) VALUES ( ?, ?)";
		$query = $this->db->query( $sql, array( $data['number'],
												$data['location']));

		$sql = "SELECT LAST_INSERT_ID();";
        $query = $this->db->query($sql);
        
        $roomId = $query->result_array()[0]["LAST_INSERT_ID()"];

		if( $isOffice) {

			$sql = "INSERT INTO Office( r_id) VALUES (?)";
			$query = $this->db->query( $sql, array( $roomId));
		}

		else {

			$sql = "INSERT INTO Sickroom(r_id, capacity) VALUES (?, ?)";
			$query = $this->db->query( $sql, array( $roomId, $data['capacity']));
		}
	}
}
?>