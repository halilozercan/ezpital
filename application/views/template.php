<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data['navigation'] = 1; $this->load->view('header', $data); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<form action='index/selcuk' method='post'>

				<input name='variable' type='text'>
				<input type='submit'>
			</form>
			<div id='body'>

				<h1>Header-1</h1>
				<h2>Header-2</h2>
				<h3>Header-3</h3>
				<h4>Header-4</h4>
				<h5>Header-5</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent diam eros, molestie eget tincidunt eu, fringilla ac augue. Proin nec odio non felis congue gravida vitae vitae risus. Ut eu sollicitudin erat. Mauris lobortis sed nisl et imperdiet. Duis vitae ultrices nibh. Cras quis lacus libero. In hac habitasse platea dictumst. Quisque congue semper eros, eu sodales nunc pharetra nec. Aliquam erat volutpat. </p>
				<ul>
					<li>Unordered List Element - 1</li>
					<li>Unordered List Element - 2</li>
					<li>Unordered List Element - 3</li>
					<li>Unordered List Element - 4</li>
				</ul>

				<ol>
					<li>Unordered List Element - 1</li>
					<li>Unordered List Element - 2</li>
					<li>Unordered List Element - 3</li>
					<li>Unordered List Element - 4</li>
				</ol>

				<a href='#'>Link</a>

				<div>Inner Div</div>
				<hr>

				<table>
					<tr>
						<td>Column - 1</td><td>Column - 2</td><td>Column - 3</td>
					</tr>
					<tr>
						<td>Cell</td><td>Cell with different Length</td><td>34</td>
					</tr>
					<tr>
						<td>Cell</td><td>Cell with different Length</td><td>34</td>
					</tr>
					<tr>
						<td>Cell</td><td>Cell with different Length</td><td>34</td>
					</tr>
					<tr>
						<td>Cell</td><td>Cell with different Length</td><td>34</td>
					</tr>
					<tr>
						<td>Cell</td><td>Cell with different Length</td><td>34</td>
					</tr>
					<tr>
						<td>Cell</td><td>Cell with different Length</td><td>34</td>
					</tr>
					<tr>
						<td>Cell</td><td>Cell with different Length</td><td>34</td>
					</tr>
				</table>
			</div>
		</div>
		
		<div id='footer'>
			&copy;cCc Shathra cCc
		</div>
	</div>
</body>
</html>