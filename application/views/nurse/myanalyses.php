<html>
<head>
<?php
echo $meta;
?>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script>

</script>
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'>Waiting Analyses</h3>

					</div>
					<div class='panel-body'>
					<?php 
					if(empty($waiting_analyses)){ 
					?>
					<div class='alert alert-info' role='alert'><strong>There is no waiting analysis!</strong></div>
					<?php 
					}else{
					?>
						<table class="table table-bordered">
							<tr>
								<th>Patient Name</th>
								<th>Report Name</th>
								<th>Date Time</th>
								<th>Actions</th>
							</tr>
						<?php foreach($waiting_analyses as $analysis){ ?>
							<tr <?php echo "style='background:#91EA8F;'"; ?>>
								<td><a <?php echo "href='viewpatient/".$analysis->examination->visit->patient->patient_id."'>".$analysis->examination->visit->patient->patient_name . " " . $analysis->examination->visit->patient->patient_surname; ?></a></td>
								<td><?php echo $analysis->analysis_name; ?></td>
								<td><?php echo $analysis->examination->date . " - " . $analysis->examination->time; ?></td>
								<td><a href="viewVisit/<?php echo $analysis->examination->visit->visit_id; ?>" class="btn btn-default">Open Visit</a></td>
							</tr>
						<?php } ?>
						</table>
					<?php } ?>
					</div>
				</div>
				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'>All Analyses</h3>

					</div>
					<div class='panel-body'>
					<?php 
					if(empty($all_analyses)){ 
					?>
					<div class='alert alert-info' role='alert'><strong>There is no analysis you have done!</strong></div>
					<?php 
					}else{
					?>
						<table class="table table-bordered">
							<tr>
								<th>Patient Name</th>
								<th>Report Name</th>
								<th>Date Time</th>
								<th>Actions</th>
							</tr>
						<?php foreach($all_analyses as $analysis){ ?>
							<tr>
								<td><a <?php echo "href='viewpatient/".$analysis->examination->visit->patient->patient_id."'>".$analysis->examination->visit->patient->patient_name . " " . $analysis->examination->visit->patient->patient_surname; ?></a></td>
								<td><?php echo $analysis->analysis_name; ?></td>
								<td><?php echo $analysis->examination->date . " - " . $analysis->examination->time; ?></td>
								<td><a href="viewVisit/<?php echo $analysis->examination->visit->visit_id; ?>" class="btn btn-default">Open Visit</a></td>
							</tr>
						<?php } ?>
						</table>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>