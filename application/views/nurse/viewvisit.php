<html>
<head>
<?php
echo $meta;
?>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script>
function setCurrentAnalysis(id, name, report) {
	document.getElementById('enterAnalysis').hidden = false;
	document.getElementById('analysisID').value = id;
	document.getElementById('reportName').innerHTML = name;
	document.getElementById('report').value = report;
}
</script>
</0head>


<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'>Analyses for Visit: <?php echo $visit_id?></h3>
					</div>
					<div class='panel-body'>
						<table class="table table-bordered">
							<tr>
								<th>Analysis ID</th>
								<th>Name</th>
								<th>Report</th>
							</tr>
							<?php foreach($analyses as $analysis){ ?>
							<tr>
								<td><?php echo $analysis->analysis_id?></td>
								<td><?php echo $analysis->analysis_name?></td>
								<td><?php
								echo $analysis->report."&nbsp;&nbsp";
								echo "<button onClick='setCurrentAnalysis(" . $analysis->analysis_id . ", \"";
								echo $analysis->analysis_name . "\", \"" . $analysis->report . "\");'>Enter</button>";
								?></td>
							</tr>
							<?php } ?>
						</table>
					</div>
				</div>
				<div class='panel panel-default' id="enterAnalysis" hidden>
					<div class='panel-heading'>
						<h3 class='panel-title'>Enter Analysis</h3>
					</div>
					<div class='panel-body'>
						<form class="basic-grey" action='../addAnalysis'
							method="POST">
							<fieldset>
								<input id="analysisID" name="analysisID" type="text"
									value='<?php echo $this->userData['id']; ?>'
									class="input-xlarge" hidden> <input id="visitID" name="visitID"
									type="text" value='<?php echo $visit_id; ?>'
									class="input-xlarge" hidden>

								<div class="control-group">
									<b>Name: </b>
									<div id='reportName'></div>
								</div>
								<div class="control-group">
									<label class="control-label" for="report">Report:</label>
									<div class="controls">
										<textarea id="report" name="report"></textarea>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="button"></label>
									<div class="controls">
										<input type="submit" class="button" value="Add" />
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>