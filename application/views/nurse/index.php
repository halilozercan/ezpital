<html>
<head>
<?php
echo $meta;
?>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script>

</script>
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
				<div>
					<h3> <?php echo $nurse->name . " " . $nurse->surname; ?> </h3>
					<hr>
					<h2><span class="label label-success">Analyses Done Today</span>  <span class="label label-info"><?php echo $analysesDoneToday; ?></span></h2>
					<h2><span class="label label-success">Waiting Analyses</span>  <span class="label label-info"><?php echo $waitingAnalyses; ?></span></h2>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>