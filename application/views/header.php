<div id='header'>

	<div id='topMenu'>

		<a id='openSideMenu' class='left' href='#'><img src='<?php echo get_instance()->config->base_url(); ?>assets/images/list91.png'></a>
		<ul class='topMenuList' id='topMenuList'>
			<li><a href='<?php echo $profile_link;?>'>Profile</a></li>
		</ul>
		<a id='openProfile' class='right' href='#'><img src='<?php echo get_instance()->config->base_url(); ?>assets/images/profile9.png'></a>
		<span class='right'><?php echo $userData['name']; ?></span>
	</div>
</div>
<div id='sideMenu' class='hidden'>
	<?php
		foreach($leftCategories as $categoryName => $categoryData){
			echo "<h3>" . $categoryName . "</h3>";
			echo "<ul id='sideMenuList'>";
			foreach($categoryData as $subCategoryName => $subCategoryLink){
				echo "<li><a href='" . $subCategoryLink . "'>" . $subCategoryName . "</a></li>";	
			}
			echo "</ul>";
		}
	?>
</div>

<div class='hidden' id='profile'>

	<?php echo $userData['username']; ?>
	<hr>
	<p><a href='<?php echo get_instance()->config->base_url(); ?>logout'>Logout</a>
</div>