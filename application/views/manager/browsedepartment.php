<html>
<head>
<?php
	echo $meta;
	$baseurl = get_instance()->config->base_url();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<?php
				if($error){
						echo "<div class='alert alert-danger' role='alert'><strong>" . $errorMessage . "</strong></div>";
				}
				?>
				<div id='add' class='panel panel-default'>

					<div class='panel-heading'><h3 class='panel-title'>Add Department</h3></div>
					<p></p>
					<form action='browseDepartment' method='post'>

						<p>Department Name: <br><input type='text' name='name'></p>
						<p><input type='submit' class='btn btn-default' value='Add'></p>
					</form>
				</div>

				<div id='browse' class='panel panel-default'>

					<?php
					echo "<div class='panel panel-default'><div class='panel-heading'><h3 class='panel-title'>Departments</h3></div><div class='panel-body'>";
					echo "<table class='table table-striped'><tr><td>Department</td><td>Number of Doctor</td><td>Actions</td></tr>";
					
					foreach($departments as $temp){

						echo "<tr><td>" . $temp['department_name'] . "</td><td>" . $temp["no"] . "</td><td>". 
							"<a class='btn btn-default' href='". $baseurl . "manager/browseDepartment?delete=" . $temp['department_id'] . "'>Delete</a>" .
							"</td></tr>";
					}
						
					echo "</table></div></div>";
					?>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>