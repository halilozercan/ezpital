<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script>

$( document ).ready(function() {

    if ($('#type').val() === 'd') {
        
        $('#doctorValues').show();
    }

    else {

    	$('#doctorValues').hide();
    }

	$('#type').change(function() {
	    if ($(this).val() === 'd') {
	        
	        $('#doctorValues').show();
	    }

	    else {

	    	$('#doctorValues').hide();
	    }
	});
});
</script>
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
				
				<div id='addUser'>
					<?php
					if($filled && $error){
						echo "<div class='alert alert-danger' role='alert'><strong>" . $errorMessage . "</strong></div>";
					}
					?>

					<?php

					if( $update_request){

					?>
					<form action="updateUser?user_id=<?php echo $update['user_id']; ?>" method="post" class="basic-grey">
					    <h1>Update User Form 
					        <span>Please fill all the the fields.</span>
					    </h1>
					    <label>
					        <span>Name :</span>
					        <input id="name" type="text" name="name" <?php if($filled) echo "value='" . $update['name'] . "'"; ?> placeholder="" />
					    </label>
					    
					    <label>
					        <span>Surname :</span>
					        <input id="surname" type="text" name="surname" <?php if($filled) echo "value='" . $update['surname'] . "'"; ?> placeholder="" />
					    </label>

					    <label>
					        <span>Email :</span>
					        <input id="email" type="text" name="email" <?php if($filled) echo "value='". $update['email'] . "'"; ?> placeholder="" />
					    </label>
					    
					    <label style='display:none;'>
					        <span>User Type :</span><select id='type' name="type">
					        	<option <?php if( $filled) if( $update['type']=="s") echo "selected"; ?> value='s'>Secretary</option>
					        	<option <?php if( $filled) if( $update['type']=="d") echo "selected"; ?> value='d'>Doctor</option>
					        	<option <?php if( $filled) if( $update['type']=="n") echo "selected"; ?> value='n'>Nurse</option>
					        	<option <?php if( $filled) if( $update['type']=="m") echo "selected"; ?> value='m'>Manager</option>
					        </select>
					    </label>

					    <span style='display:none;' id='doctorValues'>
					    <label>
					        <span>Title :</span>
					        <input id="title" type="text" name="title" <?php if($filled) echo "value='". $update['title'] . "'"; ?> placeholder="" />
					    </label>

					    <label>
					        <span>Department :</span><select id='department' name="department">
					        	<?php

					        		foreach( $update['departments'] as $temp) {

					        			echo "<option value='" . $temp['department_id'] . "' " . ($filled ? ( $temp['department_id'] == $update['department_id'] ? "selected" : ""): "") . ">" . $temp['department_name'] . "</option>";
					        		}
					        	?>
					        </select>
					    </label>
					    <label>
					        <span>Room :</span><select id='office' name="office">
					        	<?php

					        		foreach( $update['offices'] as $temp) {

					        			echo "<option value='" . $temp['room_id'] . "' ". ($filled ? ($temp['r_id'] == $update['r_id'] ? "selected" : "") : "") . ">" . $temp['number'] . "</option>";
					        		}
					        	?>
					        </select>
					    </label>
						</span>

					    <label>
					        <span>&nbsp;</span> 
					        <input type="submit" name='updateSubmit' class="button" value="Save" /> 
					    </label> 
					</form>
					<?php
					}
					else {
					?>

					<form action="addUser" method="post" class="basic-grey">
					    <h1>Add User Form 
					        <span>Please fill all the the fields.</span>
					    </h1>
					    <label>
					        <span>Username :</span>
					        <input id="username" type="text" name="username" <?php if($filled) echo "value='" . $update['username'] . "'"; ?> placeholder="Username required to login" />
					    </label>
					    <label>
					        <span>Name :</span>
					        <input id="name" type="text" name="name" <?php if($filled) echo "value='$name'"; ?> placeholder="" />
					    </label>
					    
					    <label>
					        <span>Surname :</span>
					        <input id="surname" type="text" name="surname" <?php if($filled) echo "value='$surname'"; ?> placeholder="" />
					    </label>

					    <label>
					        <span>Email :</span>
					        <input id="email" type="text" name="email" <?php if($filled) echo "value='$email'"; ?> placeholder="" />
					    </label>
					    
					    <label>
					        <span>User Type :</span><select id='type' name="type">
					        	<option <?php if( $filled) if( $type=="s") echo "selected"; ?> value='s'>Secretary</option>
					        	<option <?php if( $filled) if( $type=="d") echo "selected"; ?> value='d'>Doctor</option>
					        	<option <?php if( $filled) if( $type=="n") echo "selected"; ?> value='n'>Nurse</option>
					        	<option <?php if( $filled) if( $type=="m") echo "selected"; ?> value='m'>Manager</option>
					        </select>
					    </label>

					    <span style='display:none;' id='doctorValues'>
					    <label>
					        <span>Title :</span>
					        <input id="title" type="text" name="title" <?php if($filled) echo "value='$title'"; ?> placeholder="" />
					    </label>

					    <label>
					        <span>Department :</span><select id='department' name="department">
					        	<?php

					        		foreach( $departments as $temp) {

					        			echo "<option value='" . $temp['department_id'] . " " . ($filled ? ( $temp['department_id'] == $department['department_id'] ? "selected" : ""): "") . "'>" . $temp['department_name'] . "</option>";
					        		}
					        	?>
					        </select>
					    </label>

					    <label>
					        <span>Room :</span><select id='office' name="office">
					        	<?php

					        		foreach( $offices as $temp) {

					        			echo "<option value='" . $temp['room_id'] . "'>" . $temp['number'] . "</option>";
					        		}
					        	?>
					        </select>
					    </label>
						</span>

					    <label>
					        <span>Password :</span>
					        <input id="password" type="password" <?php if($filled) echo "value='$password'"; ?> name="password" placeholder="" />
					    </label>

					    <label>
					        <span>&nbsp;</span> 
					        <input type="submit" name='submit' class="button" value="Save" /> 
					    </label> 
					</form>
					<?php
					}
					?>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>