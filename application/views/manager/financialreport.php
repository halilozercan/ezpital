<html>
<head>
<?php
	echo $meta;
	$baseurl = get_instance()->config->base_url();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<div id='add' class='panel panel-default'>

					<div class='panel-heading'><h3 class='panel-title'>Financial Report</h3></div>
					<p></p>
					<p>Total Debt: <?php echo $result['total_debt']; ?></p>
					<p>Total Payment: <?php echo $result['total_payment']; ?></p>
					<p>Remaining Total Debt: <?php echo $result['total_debt']-$result['total_payment']; ?></p>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>