<html>
<head>
<?php
	echo $meta;
	$baseurl = get_instance()->config->base_url();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<?php
				if($error){
						echo "<div class='alert alert-danger' role='alert'><strong>" . $errorMessage . "</strong></div>";
				}
				?>

				<?php

					if( $update) {

						?>

						<div id='add' class='panel panel-default'>

							<div class='panel-heading'><h3 class='panel-title'>Update Sickroom</h3></div>
							<p></p>
							<form action='browseSickroom?update=<?php echo $updateSickroom['room_id'] ?>' method='post'>

								<p>Number: <br><input value='<?php echo $updateSickroom['number']; ?>' type='text' name='updateNumber'></p>
								<p>Location: <br><input value='<?php echo $updateSickroom['location']; ?>' type='text' name='updateLocation'></p>
								<p>Capacity: <br><input value='<?php echo $updateSickroom['capacity']; ?>' type='text' name='updateCapacity'></p>
								<p><input type='submit' name='updateSubmit' class='btn btn-default' value='Update'></p>
							</form>
						</div>

						<?php
					}
				?>
				<div id='add' class='panel panel-default'>

					<div class='panel-heading'><h3 class='panel-title'>Add a Sickroom</h3></div>
					<p></p>
					<form action='browseSickroom' method='post'>

						<p>Number: <br><input type='text' name='name'></p>
						<p>Location: <br><input type='text' name='location'></p>
						<p>Capacity: <br><input type='text' name='capacity'></p>
						<p><input type='submit' name='submit' class='btn btn-default' value='Add'></p>
					</form>
				</div>

				<div id='browse' class='panel panel-default'>

					<?php
					echo "<div class='panel panel-default'><div class='panel-heading'><h3 class='panel-title'>Sickroos</h3></div><div class='panel-body'>";
					echo "<table class='table table-striped'><tr><td>Room</td><td>Location</td><td>Capacity</td><td>Actions</td></tr>";
					
					foreach($sickrooms as $temp){

						echo "<tr><td>" . $temp['number'] . "</td><td>" . $temp["location"] . "</td>". 
							"<td>" . $temp['capacity'] . "</td>" .
							"<td><a class='btn btn-default' href='". $baseurl ."manager/browseSickroom?update=" . $temp['r_id'] . "'>Update</a> " .
							"<a class='btn btn-default' href='". $baseurl ."manager/browseSickroom?delete=" . $temp['r_id'] . "'>Delete</a>" .
							"</td></tr>";
					}
						
					echo "</table></div></div>";
					?>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>