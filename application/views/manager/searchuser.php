<html>
<head>
<?php
	echo $meta;
	$baseurl = get_instance()->config->base_url();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
				<div id='search' class='panel panel-default'>

					<div class='panel-heading'><h3 class='panel-title'>Search</h3></div>
					<p></p>
					<form action='searchuser' method='post'>

						<input type='text' name='search'> <input type='submit' class='btn btn-default' value='Search'>
					</form>

					<?php

						if( $searchDone) {

							echo "<p>". $keyword . "  <a href='searchuser'>X</a></p>";
						}
					?>
				</div>

				<?php

					if(empty($allUsers) && $searchDone){
						echo "<div class='alert alert-danger' role='alert'><strong>No result found!</strong></div>";
					}
					else{
						echo "<div class='panel panel-default'><div class='panel-heading'><h3 class='panel-title'>Users</h3></div><div class='panel-body'>";
						echo "<table class='table table-striped'><tr><td>Username</td><td>Name Surname</td><td>Type</td><td>Actions</td></tr>";
						
						foreach($allUsers as $user){

							switch( $user['type']) {

								case 'm':
									$type = "Manager";
								break;

								case 's':
									$type = "Secretary";
								break;

								case 'd':
									$type = "Doctor";
								break;

								case 'n':
									$type = "Nurse";
								break;

								default:
									$type = "Unknown";
							}
							echo "<tr><td>" . $user['username'] . "</td><td>" . $user["name"] . " " . $user["surname"] . "</td><td>" . $type . 
								"</td><td><a class='btn btn-default' href='". $baseurl . "manager/updateUser?user_id=" . $user['user_id'] . 
								"'>Update</a> <a class='btn btn-default' href='". $baseurl . "manager/deleteUser?user_id=" . $user['user_id'] . "'>Delete</a></td>"
								. "</tr>";
						}
							
						echo "</table></div></div>";
					}
				?>
				
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>