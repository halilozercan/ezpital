<html>
<head>
<?php
	echo $meta;
	$baseurl = get_instance()->config->base_url();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<div id='add' class='panel panel-default'>

					<div class='panel-heading'><h3 class='panel-title'>Doctor Report</h3></div>
					<p></p>
					<table class="table table-bordered">
						<tr>
							<td>Doctor Name</td>
							<td>Patient Count</td>
							<td>Treatment Count</td>
						</tr>
						<?php
						foreach($results as $result){
						?>
						<tr>
							<td><?php echo $result["name"] . " " . $result["surname"]; ?></td>
							<td><?php echo $result["patient_count"]; ?></td>
							<td><?php echo $result["examination_count"]; ?></td>
						</tr>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>