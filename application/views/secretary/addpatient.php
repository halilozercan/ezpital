<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<form action="" method="post" class="basic-grey">
				    <h1>Add Patient Form 
				        <span>Please fill all the the fields.</span>
				    </h1>
				    <label>
				        <span>Name :</span>
				        <input id="name" type="text" name="name" placeholder="Full First Name" />
				    </label>
				    
				    <label>
				        <span>Surname :</span>
				        <input id="surname" type="text" name="surname" placeholder="" />
				    </label>

				    <label>
				        <span>NationalID :</span>
				        <input id="nationalid" type="text" name="nationalid" placeholder="Valid National ID" />
				    </label>
				    
				    <label>
				        <span>Birth Date :</span>
				        <input id="birthdate" type="date" name="birthdate" />
				    </label>

				    <label>
				        <span>Blood Type :</span><select name="bloodtype">
				        <?php
							foreach($bloodtypes as $bloodtype){
								echo "<option value='" . $bloodtype . "'>" . $bloodtype . "</option>";
							}
						?>
				        </select>
				    </label>  

				    <label>
				        <span>Insurance Type :</span><select name="insurancetype">
				        <?php
							foreach($insurancetypes as $insurance){
								echo "<option value='" . $insurance->insurance_id . "'>" . $insurance->insurance_name . "</option>";
							}
						?>
				        </select>
				    </label>  

				    <label>
				        <span>Insurance Number :</span>
				        <input id="insurancenumber" type="text" name="insurancenumber" placeholder="Valid Insurance Number" />
				    </label>

				    <label>
				        <span>Phone :</span>
				        <input id="phone" type="text" name="phone" placeholder="Full Phone Number" />
				    </label>

				    <label>
				        <span>Address :</span>
				        <textarea id="address" name="address" placeholder="Full Address"></textarea>
				    </label> 

				    <label>
				        <span>&nbsp;</span> 
				        <input type="submit" name='submit' class="button" value="Save" /> 
				    </label> 
				</form>
				<?php 
					if(isset($alert)){
						echo "<div class='alert alert-" . $alert['type'] . "' role='alert'>" . $alert['message'] . "</div>";
					}
				?>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>