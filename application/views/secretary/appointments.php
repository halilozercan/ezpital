<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<form action="" method="get" class="basic-grey">
				    <h1>Find Appointment
				        <!--<span>Please fill all the the fields.</span>-->
				    </h1>

				    <label>
				        <span>National ID :</span>
				        <input id="national_id" type="text" name="national_id" placeholder="" <?php if($search_done) echo "value='$search_national_id'"; ?>/>
				    </label>

				    <label>
				        <span>Name :</span>
				        <input id="name" type="text" name="name" placeholder="" <?php if($search_done) echo "value='$search_name'"; ?>/>
				    </label>
				    
				    <label>
				        <span>Surname :</span>
				        <input id="surname" type="text" name="surname" placeholder="" <?php if($search_done) echo "value='$search_surname'"; ?>/>
				    </label>

				    <label>
				        <span>&nbsp;</span> 
				        <input type="submit" name="submit" class="button" value="Search" /> 
				    </label> 
				</form>

				<?php
					if($search_done && empty($search_result)){
						echo "<div class='alert alert-danger' role='alert'><strong>No result found!</strong></div>";
					}
					else if($search_done){?>
						<div class='panel panel-default'>
							<div class='panel-heading'>
								<h3 class='panel-title'>Search Results</h3>
							</div>
							<div class='panel-body'>
								<table class='table table-striped'>
									<tr>
										<td>National ID</td><td>Name</td><td>Doctor</td><td>Actions</td>
									</tr>
						
								<?php foreach($search_result as $appointment){ ?>
									<tr>
										<td><?php if($appointment->patient) {echo "<a href='viewpatient/" . $appointment->patient->patient_id . "'>";} echo $appointment->national_id; ?></td> 
										<td><?php echo $appointment->a_name . " " . $appointment->a_surname ?> </td> 
										<td><?php echo $appointment->doctor->name . " " . $appointment->doctor->surname; ?></td> 
										<td><?php 
												echo "<a href='doneAppointment/" . $appointment->appointment_id . "'>Arrived</a>";
											 ?></td></tr>
								<?php } ?>
									
								</table>
							</div>
						</div>
					<?php
					}
					?>
				
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>