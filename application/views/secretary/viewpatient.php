<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script>
$(document).ready(function(){

	$("a[name='add_payment']").click(function(){
		$("#visit_id").val( $(this).attr('id') );
	});
});
</script>
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
			<?php if(isset($alert)){ ?>
				<div class='alert alert-<?php echo $alert['type']; ?>' role='alert'><strong><?php echo $alert["message"]; ?></strong></div>
			<?php } ?>
				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'><?php echo $patient["patient_name"] . " " . $patient["patient_surname"]; ?> - Patient</h3>

					</div>
					<div class='panel-body'>
						<table class="table table-bordered">
							<tr>
								<td>National ID</td>
								<td><?php echo $patient["national_id"]; ?></td>
							</tr>
							<tr>
								<td>Name & Surname</td>
								<td><?php echo $patient["patient_name"] . " " . $patient["patient_surname"]; ?></td>
							</tr>
							<tr>
								<td>Birth Date</td>
								<td><?php echo date("d-m-Y", strtotime($patient["birth_date"])); ?></td>
							</tr>
							<tr>
								<td>Blood Type</td>
								<td><?php echo $patient["blood_type"]; ?></td>
							</tr>
							<tr>
								<td>Insurance</td>
								<td><?php echo $patient["insurance_name"]; ?></td>
							</tr>
							<tr>
								<td>Insurance ID</td>
								<td><?php echo $patient["insurance_number"]; ?></td>
							</tr>
							<tr>
								<td>Phone</td>
								<td><?php echo $patient["telephone"]; ?></td>
							</tr>
							<tr>
								<td>Address</td>
								<td><?php echo $patient["address"]; ?></td>
							</tr>
						</table>
					</div>
				</div>

				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'><?php echo $patient["patient_name"] . " " . $patient["patient_surname"]; ?> - Visits</h3>

					</div>
					<div class='panel-body'>
						<?php 
							if(empty($visits)){
								?>
								<div class='alert alert-warning' role='alert'><strong>There is no visit in the records for this patient!</strong></div>
								<?php
							}
							else{
							?>
						<table class="table table-bordered">
							<tr>
								<th>Doctor</th><th>Department</th><th>Enter Date Time</th><th>Leave Date Time</th><th>Reason</th><th>Actions</th>
							</tr>
								<?php 
								foreach($visits as $visit){
								?>
								
								<tr <?php if(!isset($visit->end_date)) echo "style='background:#91EA8F;'"; ?>>
									<td><?php echo $visit->doctor->name . " " . $visit->doctor->surname; ?></td>
									<td><?php echo $visit->doctor->department->department_name; ?></td>
									<td><?php echo $visit->enter_date . " - " . $visit->enter_time; ?></td>
									<td><?php if($visit->end_date == NULL) echo "Visit Active"; else echo $visit->end_date . " - " . $visit->end_time; ?></td>
									<td><?php echo $visit->reason; ?></td>
									<td><a href="#addPaymentForm" id="<?php echo $visit->visit_id; ?>" name="add_payment" class="btn btn-default">Add Payment</td>
								</tr>
								<?php 
								} 
							}?>
						</table>
						<a href="#openAddVisit" class="btn btn-default">Add Visit</a>
					</div>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>

	<div id="openAddVisit" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form action="../addvisit/<?php echo $patient["patient_id"]; ?>" method="post" class="basic-grey">
			    <h1>Add Visit
			        <!--<span>Please fill all the the fields.</span>-->
			    </h1>
			    <label>
			        <span>Reason :</span>
			        <textarea id="reason" name="reason" placeholder=""></textarea>
			    </label>
			    
			    <label>
			        <span>Department :</span><select id='department' name="department">
			        	<?php

			        		foreach( $departments as $temp) {

			        			echo "<option value='" . $temp['department_id'] . "'>" . $temp['department_name'] . "</option>";
			        		}
			        	?>
			        </select>
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <input type="submit" name="submit" class="button" value="Save" /> 
			    </label>
			</form>
		</div>
	</div>

	<div id="addPaymentForm" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form action="../addpayment" method="get" class="basic-grey">
			    <h1>Add Payment
			        <span>Current reamining debt: </span><a id="remaining_debt"></a>
			    </h1>
			    <input type="hidden" id="visit_id" name="visit_id">
			    <label>
			        <span>Payment Amount :</span>
			        <input id="amount" type="text" name="amount" placeholder="" />
			    </label>
			    
			    <label>
			        <span>Currency :</span>
			        <input id="currency" type="text" name="currency" value="TL" disabled>
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <input type="submit" name="submit" class="button" value="Add" /> 
			    </label>
			</form>
		</div>
	</div>
</body>
</html>