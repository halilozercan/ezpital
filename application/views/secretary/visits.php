<html>
<head>
<?php
echo $meta;
?>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script>

$(document).ready(function(){
	$("#search").click(function(){
		$.ajax({
			url:"searchVisits",
			data:{
				"patient_name" : $("#patient_name").val(),
				"patient_surname" : $("#patient_surname").val(),
				"start_date" : $("#start_date").val(),
				"end_date" : $("#end_date").val()
			},
			success:function(result){
				console.log(result);

				visits = $.parseJSON(result);

				var table = "<table class='table table-bordered'><tr><td>Doctor</td><td>Patient</td><td>Date Time</td><td>Reason</td><td>Actions</td></tr>";

				$.each(visits, function( index, visit ) {
				  table += "<tr>";
				  table += "<td>" + visit.doctor.name + " " + visit.doctor.surname + "</td>";
				  table += "<td>" + visit.patient.patient_name + " " + visit.patient.patient_surname + "</td>";
				  table += "<td>" + visit.enter_date + " " + visit.enter_time + "</td>";
				  table += "<td>" + visit.reason + "</td>";
				  table += "<td><a href='#addPaymentForm' id='" + visit.visit_id + "' name='add_payment'> Add Payment</a></td>";
				  table += "</tr>";
				});
				table += "</table>";

				$("#result-table").html(table);
		  	}
		});
	});

	$("a[name='add_payment']").click(function(){
		$("#visit_id").val( $(this).attr('id') );
	});
});

</script>
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'>Visits</h3>

					</div>
					<div class='panel-body'>
						<form class="basic-grey">
						    <h1>Search Visit
						        <!--<span>Please fill all the the fields.</span>-->
						    </h1>
						    <label>
						        <span>Patient Name :</span>
						        <input type="text" id="patient_name" name="patient_name" placeholder="">
						    </label>
						    
						    <label>
						        <span>Patient Surname :</span>
						        <input type="text" id="patient_surname" name="patient_surname" placeholder="">
						    </label>

						    <label>
						        <span>Range Start Date :</span>
						        <input type="date" id="start_date" name="start_date" value="<?php echo date("Y-m-d"); ?>">
						    </label>

						    <label>
						        <span>Range End Date :</span>
						        <input type="date" id="end_date" name="end_date" value="<?php echo date("Y-m-d"); ?>">
						    </label>

						    <label>
						        <span>&nbsp;</span> 
						        <input id="search" name="search" class="button" value="Search" /> 
						    </label>
						</form>
					</div>
					<table class='table table-bordered' id="result-table">
					</table>
				</div>
				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'>All Visits with Remamining Debts</h3>

					</div>
					<div class='panel-body'>
					<?php 
					if(empty($debt_visits)){ 
					?>
					<div class='alert alert-info' role='alert'><strong>There is no visit with remaining debt!</strong></div>
					<?php 
					}else{
					?>
						<table class="table table-bordered">
							<tr>
								<th>Patient Name</th>
								<th>Date Time</th>
								<th>Remaining Paid/Debt Amount</th>
								<th>Actions</th>
							</tr>
						<?php foreach($debt_visits as $debt_visit){ ?>
							<tr>
								<td><a <?php echo "href='viewpatient/".$debt_visit->patient->patient_id."'>".$debt_visit->patient->patient_name . " " . $debt_visit->patient->patient_surname; ?></a></td>
								<td><?php echo $debt_visit->enter_date ." " . $debt_visit->enter_time; ?></td>
								<td><?php echo $debt_visit->paid ." / " . $debt_visit->debt; ?></td>
								<td><?php echo "<a href='#addPaymentForm' id='". $debt_visit->visit_id ."' name='add_payment'> Add Payment</a>"; ?></td>
							</tr>
						<?php } ?>
						</table>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>

	<div id="addPaymentForm" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form action="addpayment" method="get" class="basic-grey">
			    <h1>Add Payment
			        <span>Current reamining debt: </span><a id="remaining_debt"></a>
			    </h1>
			    <input type="hidden" id="visit_id" name="visit_id">
			    <label>
			        <span>Payment Amount :</span>
			        <input id="amount" type="text" name="amount" placeholder="" />
			    </label>
			    
			    <label>
			        <span>Currency :</span>
			        <input id="currency" type="text" name="currency" value="TL" disabled>
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <input type="submit" name="submit" class="button" value="Add" /> 
			    </label>
			</form>
		</div>
	</div>
</body>
</html>