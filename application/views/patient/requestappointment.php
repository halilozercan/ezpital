<html>
<head>
<?php
echo $meta;
?>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
  $(function() {
    $( "#datepicker" ).datepicker();

    $("#submit").click(function(){

		$(this).val("Please wait...");
		$(this).prop('disabled', true);

		$.ajax({
			url:"patient/requestAppointment",
			data:{
				name : $("#name").val(),
				surname : $("#surname").val(),
				nationalid : $("#nationalid").val(),
				date : $("#date").val(),
				time : $("#time").val(),
				department_id : $("#department_id").val(),
			},
			success:function(result){
				console.log(result);

				var obj = $.parseJSON(result);

		    	$('#result-area').html("<div class='alert alert-" + obj.condition + "' role='alert'>" + obj.message + "</div>");

		    	if(obj.condition == 'success'){
		    		$(".basic-grey").remove();
		    	}

		    	$("#submit").prop('disabled', false);
		    	$("#submit").val('Send');
		  	}
		});
    });
  });
  </script>
</head>
<body>
	<div id='container'>
		<div id='contentContainer'>
			<div id='body'>
				<div>

					<form action="" method="post" class="basic-grey">
					    <h1>Request Appointment
					        <!--<span>Please fill all the the fields.</span>-->
					    </h1>
					    <label>
					        <span>Name :</span>
					        <input id="name" type="text" name="name" placeholder="Full First Name" />
					    </label>
					    
					    <label>
					        <span>Surname :</span>
					        <input id="surname" type="text" name="surname" placeholder="" />
					    </label>

					    <label>
					        <span>National ID :</span>
					        <input id="nationalid" type="text" name="nationalid" placeholder="Valid National ID" />
					    </label>

					    <label>
					        <span>Date :</span> 
					        <input id="date" type="date" name="date" value="Save" /> 
					    </label>

					    <label>
					        <span>Time :</span> 
					        <input id="time" type="time" name="time" value="Save" /> 
					    </label>

					    <label>
					    	<span>Department :</span>
					    	<select name='department_id' id='department_id'>
							<?php
							foreach ( $departments as $department ) {
								echo "<option value=".$department['department_id'].">".$department['department_name']."</option>";
							}
							?>
							</select>
						</label>

						<label>
					        <span>&nbsp;</span> 
					        <input name='submit' id='submit' class="button" value="Send" />
					    </label> 
					</form>
					<div id='result-area'>
					</div>
				</div>
			</div>
		</div>
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>