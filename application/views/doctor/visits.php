<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<form action="" method="get" class="basic-grey">
				    <h1>Find Visit
				        <!--<span>Please fill all the the fields.</span>-->
				    </h1>
				    <label>
				        <span>Start Date :</span>
				        <input id="s_date" type="date" name="s_date" placeholder="" <?php if($search_done) echo "value='$search_s_date'"; ?>/>
				    </label>
				    
				    <label>
				        <span>End Date :</span>
				        <input id="e_date" type="date" name="e_date" placeholder="" <?php if($search_done) echo "value='$search_e_date'"; ?>/>
				    </label>

				    <label>
				        <span>&nbsp;</span> 
				        <input type="submit" name="submit" class="button" value="Search" /> 
				    </label> 
				</form>

				<?php
					if($search_done && empty($search_result)){
						echo "<div class='alert alert-danger' role='alert'><strong>No result found!</strong></div>";
					}
					else{
						echo "<div class='panel panel-default'><div class='panel-heading'><h3 class='panel-title'>Search Results</h3></div><div class='panel-body'>";
						echo "<table class='table table-striped'><tr><td>Patient Name</td><td>Date Time</td><td>Examination Count</td><td>Actions</td></tr>";
						
						foreach($search_result as $result){
							if( isset($result->end_date) )
								echo "<tr><td><a href='viewPatient/" . $result->patient->patient_id . "'>" . $result->patient->patient_name . " " . $result->patient->patient_surname . "</td><td>" . $result->enter_date . " - " . $result->enter_time .  "</td><td>" . $result->examination_count . "</td>" . 
									 "<td><a class='btn btn-default' href='visit/" . $result->visit_id . "'>View</a></td></tr>";
							else
								echo "<tr style='background:#91EA8F;'><td><a href='viewPatient/" . $result->patient->patient_id . "'>" . $result->patient->patient_name . " " . $result->patient->patient_surname . "</td><td>" . $result->enter_date . " - " . $result->enter_time .  "</td><td>" . $result->examination_count . "</td>" . 
									 "<td><a class='btn btn-default' href='visit/" . $result->visit_id . "'>View</a></td></tr>";
						}
							
						echo "</table></div></div>";
					}
				?>
				
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>