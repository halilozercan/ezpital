<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script type="text/javascript">
$(document).ready(function(){
	$( "a[id^='treatmentdetails_']" ).click(function(){
		
		var id = $(this).attr("id");
		var arr = id.split('_');
		id = arr[1];

		$("#show_story").val("Loading");
    	$("#show_diagnosis").val("Loading");
    	$("#show_treatment").val("Loading");

		$.ajax({
			url:"treatment/" + id,
			success:function(result){
				console.log(result);

				var treatment_details = $.parseJSON(result);

		    	$("#show_story").val(treatment_details.treatment.story);
		    	$("#show_diagnosis").val(treatment_details.treatment.diagnosis);
		    	$("#show_treatment").val(treatment_details.treatment.treatment);
		  	}
		});
	});
});
</script>
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<form action="" method="get" class="basic-grey">
				    <h1>Find Visit
				        <!--<span>Please fill all the the fields.</span>-->
				    </h1>
				    <label>
				        <span>Start Date :</span>
				        <input id="s_date" type="date" name="s_date" placeholder="" <?php if($search_done) echo "value='$search_s_date'"; ?>/>
				    </label>
				    
				    <label>
				        <span>End Date :</span>
				        <input id="e_date" type="date" name="e_date" placeholder="" <?php if($search_done) echo "value='$search_e_date'"; ?>/>
				    </label>

				    <label>
				        <span>&nbsp;</span> 
				        <input type="submit" name="submit" class="button" value="Search" /> 
				    </label> 
				</form>

				<?php
					if($search_done && empty($search_result)){
						echo "<div class='alert alert-danger' role='alert'><strong>No result found!</strong></div>";
					}
					else{
						echo "<div class='panel panel-default'><div class='panel-heading'><h3 class='panel-title'>Search Results</h3></div><div class='panel-body'>";
						echo "<table class='table table-striped'><tr><td>Patient Name</td><td>Date Time</td><td>Visit Reason</td><td>Actions</td></tr>";
						
						foreach($search_result as $result){
							echo "<tr><td><a href='viewPatient/" . $result->patient->patient_id . "'>" . $result->patient->patient_name . " " . $result->patient->patient_surname . "</td><td>" . $result->examination->date . " - " . $result->examination->time .  
								 "</td><td><a href='visit/" . $result->visit->visit_id . "'>" . $result->visit->reason . "</a></td>" . 
								 "<td><a href='#showTreatmentDetails' id='treatmentdetails_" . $result->treatment_id . "'>See Details</a></td></tr>";
						}
							
						echo "</table></div></div>";
					}
				?>
				
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>

	<div id="showTreatmentDetails" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form class="basic-grey">
			    <h1>Treatment Details
			        <!--<span>Please fill all the the fields.</span>-->
			    </h1>
			    <label>
			        <span>Story :</span>
			        <textarea id="show_story" name="story" placeholder="" disabled></textarea>
			    </label>
			    
			    <label>
			        <span>Diagnosis :</span>
			        <textarea id="show_diagnosis" name="diagnosis" placeholder="" disabled></textarea>
			    </label>

			    <label>
			        <span>Treatment :</span>
			        <textarea id="show_treatment" name="treatment" placeholder="" disabled></textarea>
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <a href="#close" title="Close" class="button">Close</a>
			    </label>
			</form>
		</div>
	</div>
</body>
</html>