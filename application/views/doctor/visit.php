<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script>

$(document).ready(function(){
	$( "a[id^='treatmentdetails_']" ).click(function(){
		
		var id = $(this).attr("id");
		var arr = id.split('_');
		id = arr[1];

		$("#show_story").val("Loading");
    	$("#show_diagnosis").val("Loading");
    	$("#show_treatment").val("Loading");

		$.ajax({
			url:"../treatment/" + id,
			success:function(result){
				console.log(result);

				var treatment_details = $.parseJSON(result);

		    	$("#show_story").val(treatment_details.treatment.story);
		    	$("#show_diagnosis").val(treatment_details.treatment.diagnosis);
		    	$("#show_treatment").val(treatment_details.treatment.treatment);
		  	}
		});
	});	

	$( "a[id^='analysisdetails_']" ).click(function(){
		
		var id = $(this).attr("id");
		var arr = id.split('_');
		id = arr[1];

		$("#show_analysis_name").val("Loading");
    	$("#show_report").val("Loading");
    	$("#show_nurse").val("Loading");

		$.ajax({
			url:"../analysis/" + id,
			success:function(result){
				console.log(result);

				var analysis_details = $.parseJSON(result);

		    	$("#show_analysis_name").val(analysis_details.analysis.analysis_name);
		    	$("#show_report").val(analysis_details.analysis.report);
		    	$("#show_nurse").val(analysis_details.analysis.nurse.name + " " + analysis_details.analysis.nurse.surname);
		  	}
		});
	});	
});

</script>
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
			<?php if(isset($visit->end_date)){ ?>
				<div class='alert alert-danger' role='alert'>This visit is closed. No one can do any changes on it.</div>
			<?php }
			if($found){ ?>
				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'><?php echo $visit->patient->patient_name . " " . $visit->patient->patient_surname; ?> - Patient</h3>

					</div>
					<div class='panel-body'>
						<table class="table table-bordered">
							<tr>
								<td>Name & Surname</td>
								<td><?php echo $visit->patient->patient_name . " " . $visit->patient->patient_surname; ?></td>
							</tr>
							<tr>
								<td>Visit Date</td>
								<td><?php echo $visit->enter_date . " " . $visit->enter_time; ?></td>
							</tr>
							<tr>
								<td>Department</td>
								<td><?php echo $visit->doctor->department->department_name; ?></td>
							</tr>
							<tr>
								<td>Doctor</td>
								<td><?php echo $visit->doctor->name . " " . $visit->doctor->surname; ?></td>
							</tr>
							<tr>
								<td>Reason</td>
								<td><?php echo $visit->reason; ?></td>
							</tr>
							<tr>
								<td></td>
								<td>
								<?php if(!isset($visit->end_date)){ ?>
									<a class="btn btn-lg btn-default" href="../doneVisit/<?php echo $visit->visit_id; ?>" style="float:right;">Close this Visit</a>
								<?php } ?>
								</td>
							</tr>
							
						</table>
					</div>
				</div>

				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'>Treatments</h3>
						<?php if($visit->doctor->doctor_id == $userData['id'] && !isset($visit->end_date)) { ?><a href="#openAddTreatment" class="btn btn-default">Add Treatment</a><?php } ?>
					</div>
					<div class='panel-body'>
					<?php if( !empty($treatments) ){ ?>
						<table class="table table-bordered">
							<tr>
								<th>Doctor</th><th>Department</th><th>Date Time</th><th>Actions</th>
							</tr>
							<?php foreach($treatments as $treatment){ ?>
							<tr>
								<td><?php echo $treatment->doctor->name . " " . $treatment->doctor->surname; ?></td>
								<td><?php echo $treatment->doctor->department->department_name; ?></td>
								<td><?php echo $treatment->examination->date . " - " . $treatment->examination->time; ?></td>
								<td><a href="#showTreatmentDetails" id="treatmentdetails_<?php echo $treatment->treatment_id; ?>">See Details</a></td>
							</tr>
							<?php } ?>
						</table>
					<?php }else{ ?>
						<div class='alert alert-warning' role='alert'>There are no treatments for this visit</div>
					<?php } ?>
					</div>
				</div>

				<div class='panel panel-default'>
					<div class='panel-heading'>
						<h3 class='panel-title'>Analysis</h3>
						<?php if($visit->doctor->doctor_id == $userData['id'] && !isset($visit->end_date)) { ?><a href="#openAddAnalysis" class="btn btn-default">Request Analysis</a><?php } ?>
					</div>
					<div class='panel-body'>
					<?php if( !empty($analysises) ){ ?>
						<table class="table table-bordered">
							<tr>
								<th>Nurse</th><th>Analysis Name</th><th>Date Time</th><th>Actions</th>
							</tr>
							<?php foreach($analysises as $analysis){ ?>
							<tr <?php if( !isset( $analysis->report) ) echo "style='background:#E58686;'"; else echo "stlye='background:#86E5C5;'" ?>>
								<td><?php echo $analysis->nurse->name . " " . $analysis->nurse->surname; ?></td>
								<td><?php echo $analysis->analysis_name; ?></td>
								<td><?php echo $analysis->examination->date . " - " . $analysis->examination->time; ?></td>
								<td><a href="#showAnalysisDetails" id="analysisdetails_<?php echo $analysis->analysis_id; ?>">See Details</a></td>
							</tr>
							<?php } ?>
						</table>
					<?php }else{ ?>
						<div class='alert alert-warning' role='alert'>There is no analysis for this visit</div>
					<?php } ?>
					</div>
				</div>
			<?php }else{ ?>
				<div class='alert alert-<?php echo $alert['type']; ?>' role='alert'><strong><?php echo $alert["message"]; ?></strong></div>
			<?php } ?>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>

	<div id="openAddTreatment" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form action="../addtreatment/<?php echo $visit->visit_id; ?>" method="post" class="basic-grey">
			    <h1>Add Treatment
			        <!--<span>Please fill all the the fields.</span>-->
			    </h1>
			    <label>
			        <span>Story :</span>
			        <textarea id="story" name="story" placeholder=""></textarea>
			    </label>
			    
			    <label>
			        <span>Diagnosis :</span>
			        <textarea id="diagnosis" name="diagnosis" placeholder=""></textarea>
			    </label>

			    <label>
			        <span>Treatment :</span>
			        <textarea id="treatment" name="treatment" placeholder=""></textarea>
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <input type="submit" name="submit" class="button" value="Save" /> 
			    </label>
			</form>
		</div>
	</div>

	<div id="openAddAnalysis" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form action="../addanalysis/<?php echo $visit->visit_id; ?>" method="post" class="basic-grey">
			    <h1>Add Treatment
			        <!--<span>Please fill all the the fields.</span>-->
			    </h1>
			    <label>
			        <span>Report Name :</span>
			        <input type="text" id="analysis_name" name="analysis_name" placeholder="">
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <input type="submit" name="submit" class="button" value="Request" /> 
			    </label>
			</form>
		</div>
	</div>

	<div id="showTreatmentDetails" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form class="basic-grey">
			    <h1>Treatment Details
			        <!--<span>Please fill all the the fields.</span>-->
			    </h1>
			    <label>
			        <span>Story :</span>
			        <textarea id="show_story" name="story" placeholder="" disabled></textarea>
			    </label>
			    
			    <label>
			        <span>Diagnosis :</span>
			        <textarea id="show_diagnosis" name="diagnosis" placeholder="" disabled></textarea>
			    </label>

			    <label>
			        <span>Treatment :</span>
			        <textarea id="show_treatment" name="treatment" placeholder="" disabled></textarea>
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <a href="#close" title="Close" class="button">Close</a>
			    </label>
			</form>
		</div>
	</div>

	<div id="showAnalysisDetails" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form class="basic-grey">
			    <h1>Analysis Details
			        <!--<span>Please fill all the the fields.</span>-->
			    </h1>
			    <label>
			        <span>Report Name :</span>
			        <input type="text" id="show_analysis_name" name="analysis_name" placeholder="" disabled>
			    </label>
			    
			    <label>
			        <span>Report :</span>
			        <textarea id="show_report" name="report" placeholder="" disabled></textarea>
			    </label>

			    <label>
			        <span>Nurse :</span>
			        <input type="text" id="show_nurse" name="nurse" placeholder="" disabled>
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <a href="#close" title="Close" class="button">Close</a>
			    </label>
			</form>
		</div>
	</div>
</body>
</html>