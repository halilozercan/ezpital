<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>

				<form action="" method="get" class="basic-grey">
				    <h1>Find Patient
				        <!--<span>Please fill all the the fields.</span>-->
				    </h1>
				    <label>
				        <span>Name :</span>
				        <input id="name" type="text" name="name" placeholder="" <?php if($searchDone) echo "value='$searchName'"; ?>/>
				    </label>
				    
				    <label>
				        <span>Surname :</span>
				        <input id="surname" type="text" name="surname" placeholder="" <?php if($searchDone) echo "value='$searchSurname'"; ?>/>
				    </label>

				    <label>
				        <span>Inpatient :</span><select name="inpatient">
				        	<option value='0' <?php if($searchDone && $searchInpatient=='0') echo "selected"; ?>>Both</option>
				        	<option value='1' <?php if($searchDone && $searchInpatient=='1') echo "selected"; ?>>Inpatient</option>
				        	<option value='2' <?php if($searchDone && $searchInpatient=='2') echo "selected"; ?>>Not Inpatient</option>
				        </select>
				    </label>

				    <label>
				        <span>&nbsp;</span> 
				        <input type="submit" name="submit" class="button" value="Search" /> 
				    </label> 
				</form>

				<?php
					if($searchDone && empty($searchResult)){
						echo "<div class='alert alert-danger' role='alert'><strong>No result found!</strong></div>";
					}
					else if($searchDone){
						echo "<div class='panel panel-default'><div class='panel-heading'><h3 class='panel-title'>Search Results</h3></div><div class='panel-body'>";
						echo "<table class='table table-striped'><tr><td>National ID</td><td>Name</td><td>Last Visited</td></tr>";
						
						foreach($searchResult as $patient){
							echo "<tr><td><a href='viewpatient/" . $patient->patient_id . "'>" . $patient->national_id . "</td><td>" . $patient->patient_name . " " . $patient->patient_surname . "</td><td>" . $patient->last_visit->enter_date . "</td><td><a href='viewpatient/" . $patient->patient_id . "'>View</a></td></tr>";
						}
							
						echo "</table></div></div>";
					}
				?>
				
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>