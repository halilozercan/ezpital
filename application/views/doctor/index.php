<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script>

</script>
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
				<div>
					<h3> <?php echo $doctor->title . " " . $doctor->name . " " . $doctor->surname . " - " . $doctor->department->department_name; ?> </h3>
					<hr>
					<h2><span class="label label-success">Patients Treated Today</span>  <span class="label label-info"><?php echo $patientsTreatedToday; ?></span></h2>
					<h2><span class="label label-success">Patients Treated This Week</span>  <span class="label label-info"><?php echo $patientsTreatedThisWeek; ?></span></h2>
					<br>
					<h2><span class="label label-success">Remaining Appointment Count</span>  <span class="label label-info"><?php echo $remainingAppointments; ?></span></h2>
				</div>

				<div class="tabs">
				    <ul class="nav nav-tabs" role="tablist">
				        <li role="presentation" class="active"><a href="#tab1">Last Patients</a></li>
				    </ul>
				 
				    <div class="tab-content">
				        <div id="tab1" class="tab active">
				            <table class="table table-bordered">
				            	<tr>
				            		<td>Name</td><td>Visit Reason</td><td>Waiting Analysis</td><td>Visit Active</td>
				            	</tr>
				            	<?php 
				            	foreach($last_visits as $visit){
				            		echo "<tr><td>" . $visit->patient->patient_name . " " . $visit->patient->patient_surname . "</td><td>" . $visit->reason . "</td><td>" . $visit->waiting_analysis . "</td><td>" . $visit->is_active . "</td></tr>";
				            	}
				            	?>
				            </table>
				        </div>
				    </div>
				</div>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>
</body>
</html>