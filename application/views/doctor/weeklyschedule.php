<html>
<head>
<?php
	echo $meta;
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
	<?php $data = $headerData; $this->load->view('header', $headerData); ?>
	
	<div id='container'>
		<div id='contentContainer'>

			<div id='body'>
			<?php
				if(isset($alert)){
			?>
				<div class='alert alert-<?php echo $alert['type']; ?>' role='alert'><strong><?php echo $alert["message"]; ?></strong></div>
			<?php } ?>
			<?php 
				foreach($weekly_schedule as $working_time){
					echo "<p>" . $week_days[$working_time->day] . " " . $working_time->start_time . " - " . $working_time->end_time . " <a href='cancelWorkingHour/" . $working_time->id . "'>Cancel</a></p>";
				}
			?>
			<a href="#openAddWorkingTime" class="btn btn-default">Add New Working Time</a>
			</div>
		</div>
		
		<?php $this->load->view('footer'); ?>
	</div>

	<div id="openAddWorkingTime" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<form action="addWorkingHour" method="post" class="basic-grey">
			    <h1>Add a time period
			        <span>Please enter a time period that you are going to work in.</span>
			    </h1>
			    <label>
			        <span>Day :</span>
			        <select id="day" name="day">
			        <?php
			        	foreach($week_days as $key => $value)
			        		echo "<option value='" . $key . "'>" . $value . "</option>";
			        ?>
			        </select>
			    </label>

			    <label>
			        <span>Start Time :</span>
			        <input type="time" id="start_time" name="start_time" placeholder="">
			    </label>

			    <label>
			        <span>End Time :</span>
			        <input type="time" id="end_time" name="end_time" placeholder="">
			    </label>

			    <label>
			        <span>Capacity :</span>
			        <input type="number" id="capacity" name="capacity" placeholder="Between 0 and 100">
			    </label>

			    <label>
			        <span>&nbsp;</span> 
			        <input type="submit" name="submit" class="button" value="Save" /> 
			    </label>
			</form>
		</div>
	</div>
</body>
</html>