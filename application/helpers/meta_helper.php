<?php
/*
This class is designed to implement helper functions to build html meta tag 
from controllers.
*/
function asset_url(){

	return get_instance()->config->base_url() . "assets/";
}

/*
By passing $data['meta'] as $metaData to this function,
you can add a javascript file to head tag with specified name.

Warning: do not include ".js" in file name. Function does it automatically. 
*/

function loadJs($js, &$metaData){

	$metaData .= "<script type='text/javascript' src=\"" . asset_url() . "js/" . $js . ".js\"></script>\n";
}

function loadCss($css, &$metaData){
	
	$metaData .= "<link rel='stylesheet' href=\"" . asset_url() . "css/" . $css . ".css\">\n";
}

?>