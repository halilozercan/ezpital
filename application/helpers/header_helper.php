<?php
/*
This class is designed to implement helper functions to build header variables
from controllers.
*/
function prepare_header(&$headerData, $userData){

	$baseurl = get_instance()->config->base_url();

	if( isset($userData['userType']) ){

		if($userData['userType'] == 'd'){

			$leftCategories = array("Patient" => array("Find a Patient" => $baseurl . "doctor/findPatient", "Visits" => $baseurl . "doctor/visits"), 
									"Examinations" => array("My Treatments" => $baseurl . "doctor/treatments"), 
									"Schedule" => array("Weekly Schedule" => $baseurl . "doctor/weeklySchedule"));
		}
		else if($userData['userType'] == 'n'){
			$leftCategories = array("Patient" => array("Find a Patient" => $baseurl . "nurse/findPatient"),
							  		"Analysis" => array("My Analyses" => $baseurl. "nurse/myAnalyses"));
		}
		else if($userData['userType'] == 's'){

			$leftCategories = array("Patient" => array("Browse Patients" => $baseurl . "secretary/findPatient", "Add a Patient" => $baseurl . "secretary/addPatient"), 
									"Visits" => array("Browse Visits" => $baseurl . "secretary/visits"), 
									"Appointments" => array("Browse Appointments" => $baseurl . "secretary/appointments"));	
		}
		else if($userData['userType'] == 'm'){

			$leftCategories = array("User" => array("Browse Users" => $baseurl . "manager/searchUser", 
													"Add a User" => $baseurl . "manager/addUser"),
									"Room" => array("Browse Offices" => $baseurl . "manager/browseOffice",
													"Browse Sickrooms" => $baseurl . "manager/browseSickroom"),
									"Department" => array( "Browse Departments" => $baseurl . "manager/browseDepartment"),
									"Report" => array( "Doctor Report" => $baseurl . "manager/doctorReport",
													   "Financial Report" => $baseurl . "manager/financialReport"));	
		}
	}

	$profile_link = $baseurl;
	$headerData = array("userData" => $userData, "leftCategories" => $leftCategories, "profile_link" =>$profile_link);
	
}

function redirectToHome($type){
	if($type == 'd'){
		redirect('/doctor');
	}
	else if($type == 'n'){
		redirect('/nurse');
	}
	else if($type == 's'){
		redirect('/secretary');
	}
	else if($type == 'm'){
		redirect('/manager');
	}
	else if($type == 'p'){
		redirect('/patient');
	}
}

function getBloodTypes(){

	return array('A-', 'A+', '0-', '0+', 'AB-', 'AB+', 'B-', 'B+');
}

function create_alert($type, $message){
	return array("type" => $type, "message" => $message);
}

function get_week_days(){
	return array("1" => "Monday",
		  "2" => "Tuesday",
		  "3" => "Wednesday",
		  "4" => "Thursday",
		  "5" => "Friday",
		  "6" => "Saturday",
		  "7" => "Sunday");
}

?>
