<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doctor_controller extends MY_Controller {

	function __construct()
	{
	    parent::__construct();

	    if( $this->userData['userType'] != 'd' ){
			redirectToHome($this->userData['userType']);
		}

		$this->data['meta'] = "";

		loadJs("jquery-1.11.1.min", $this->data['meta']);
		loadJs("ui", $this->data['meta']);
		loadJs("tabs", $this->data['meta']);
		loadCss("style", $this->data['meta']);
		loadCss('basicgrey', $this->data['meta']);
		loadCss('bootstrap.min', $this->data['meta']);
		loadCss('bootstrap-theme.min', $this->data['meta']);
		loadCss("tabs", $this->data['meta']);

	}

	public function index()
	{

		$this->load->model('doctor_model', '', TRUE);
		$mDoctor = $this->doctor_model->get_doctor($this->userData['id']);
		$this->data['doctor'] = $mDoctor;

		$this->data['patientsTreatedToday'] = $mDoctor->get_today_patient_count();
		$this->data['patientsTreatedThisWeek'] = $mDoctor->get_week_patient_count();
		$this->data['remainingAppointments'] = $mDoctor->get_remaining_appointment_count();

		$this->data['last_visits'] = $mDoctor->get_last_visits(5);
		//$this->data['remaining_appointments'] = $mDoctor->get_remaining_appointments();

		$this->load->view('doctor/index', $this->data);

	}

	public function findPatient(){
		$this->data["searchDone"] = FALSE;

		if($this->input->get("submit")){

			$name = $this->input->get("name");
			$surname = $this->input->get("surname");
			$inpatient = $this->input->get("inpatient");

			$this->load->model("patient_model", "patient", TRUE);

			$this->data["searchResult"] = $this->patient->find_patients($name, $surname, $inpatient);
			$this->data["searchDone"] = TRUE;
			$this->data["searchName"] = $name;
			$this->data["searchSurname"] = $surname;
			$this->data["searchInpatient"] = $inpatient;

		}

		$this->load->view('doctor/findpatient', $this->data);
	}

	public function visits(){

		$this->data["search_done"] = FALSE;
		$this->load->model("visit_model", "", TRUE);

		if($this->input->get("submit")){

			$s_date = $this->input->get("s_date");
			$e_date = $this->input->get("e_date");
			
			$this->data["search_result"] = $this->visit_model->get_visits_doctor($this->userData['id'], $s_date, $e_date);
			$this->data["search_done"] = TRUE;
			$this->data["search_s_date"] = $s_date;
			$this->data["search_e_date"] = $e_date;

		}
		else{
			$this->data["search_result"] = $this->visit_model->get_visits_doctor($this->userData['id']);
		}

		$this->load->view('doctor/visits', $this->data);
	}

	public function treatments(){

		$this->data["search_done"] = FALSE;
		$this->load->model("treatment_model", "", TRUE);

		if($this->input->get("submit")){

			$s_date = $this->input->get("s_date");
			$e_date = $this->input->get("e_date");
			
			$this->data["search_result"] = $this->treatment_model->get_treatments_doctor($this->userData['id'], $s_date, $e_date);
			$this->data["search_done"] = TRUE;
			$this->data["search_s_date"] = $s_date;
			$this->data["search_e_date"] = $e_date;

		}
		else{
			$this->data["search_result"] = $this->treatment_model->get_treatments_doctor($this->userData['id']);
		}

		$this->load->view('doctor/treatments', $this->data);
	}

	public function viewPatient($patient_id){
		$this->load->model("patient_model", "patient", TRUE);
		$this->load->model("visit_model", "visit", TRUE);
		$this->data['found'] = FALSE;

		$mPatient = $this->patient->get_patient($patient_id);
		$mVisits = $this->visit->get_visits_patient($patient_id);
		
		if( isset($mPatient->national_id) ){
			$this->data['found'] = TRUE;
			$this->data['patient'] = $mPatient->as_array();
			$this->data['visits'] = $mVisits;
		}
		else{
			$this->data['alert'] = create_alert('danger', 'The specified patient was not found!');
		}
		$this->load->view('doctor/viewpatient', $this->data);
	}

	public function visit($visit_id){
		$this->load->model("treatment_model", "", TRUE);
		$this->load->model("analysis_model", "", TRUE);
		$this->load->model("visit_model", "visit", TRUE);
		$this->data['found'] = FALSE;

		$mVisit = $this->visit->get_visit($visit_id);
		$mTreatments = $this->treatment_model->get_treatments_visit($visit_id);
		$mAnalysises = $this->analysis_model->get_analysises_visit($visit_id);

		if( isset($mVisit->visit_id) ){
			$this->data['found'] = TRUE;
			$this->data['visit'] = $mVisit;
			$this->data['treatments'] = $mTreatments;
			$this->data['analysises'] = $mAnalysises;

		}
		else{
			$this->data['alert'] = create_alert('danger', 'The specified visit was not found!');
		}

		$this->load->view('doctor/visit', $this->data);
	}

	public function doneVisit($visit_id){
		$this->load->model("patient_model", "patient", TRUE);
		$this->load->model("visit_model", "visit", TRUE);

		$mVisit = $this->visit->get_visit($visit_id);
		
		if( isset($mVisit->visit_id) && !isset($mVisit->end_date) ){
			
			$this->visit->done_visit($visit_id);
			redirect('doctor/visit/' . $visit_id);
		}
		else{
			redirect('doctor/visits');
		}
		
	}

	public function treatment($treatment_id){
		if($this->input->is_ajax_request()){
			$this->load->model("treatment_model", "", TRUE);

			$mTreatment = $this->treatment_model->get_treatment($treatment_id);

			if( isset($mTreatment->treatment_id) ){
				$result['success'] = TRUE;
				$result['treatment'] = $mTreatment;

				echo json_encode($result);

			}
			else{
				$result['success'] = FALSE;

				echo json_encode($result);
			}
			exit;	
		}
		
	}

	public function analysis($analysis_id){
		if($this->input->is_ajax_request()){
			$this->load->model("analysis_model", "", TRUE);

			$mAnalysis = $this->analysis_model->get_analysis($analysis_id);

			if( isset($mAnalysis->analysis_id) ){
				$result['success'] = TRUE;
				$result['analysis'] = $mAnalysis;

				echo json_encode($result);

			}
			else{
				$result['success'] = FALSE;

				echo json_encode($result);
			}
			exit;	
		}
		
	}

	public function addtreatment($visit_id){

		$this->load->model("examination_model", "", TRUE);
		$this->load->model("treatment_model", "", TRUE);
		$this->load->model("visit_model", "visit", TRUE);
		$this->data['found'] = FALSE;

		$mVisit = $this->visit->get_visit($visit_id);

		if( isset($mVisit->visit_id) ){
			
			$e_id = $this->examination_model->insert_examination($mVisit->visit_id);
			$t_id = $this->treatment_model->insert_treatment(array("e_id" => $e_id,
														   "story" => $this->input->post("story"),
														   "diagnosis" => $this->input->post("diagnosis"),
														   "treatment" => $this->input->post("treatment"),
														   "doctor_id" => $this->userData["id"]));

			//$this->data['alert'] = create_alert('success', 'Treatment is added succesfully!');
		}
		else{
			//$this->data['alert'] = create_alert('danger', 'The specified visit was not found!');
		}

		redirect("doctor/visit/" . $visit_id);
	}

	public function addanalysis($visit_id){

		$this->load->model("examination_model", "", TRUE);
		$this->load->model("analysis_model", "", TRUE);
		$this->load->model("visit_model", "visit", TRUE);
		$this->data['found'] = FALSE;

		$mVisit = $this->visit->get_visit($visit_id);

		if( isset($mVisit->visit_id) ){
			
			$e_id = $this->examination_model->insert_examination($mVisit->visit_id);
			$analysis_id = $this->analysis_model->insert_analysis_doctor(array("e_id" => $e_id,
														   				"analysis_name" => $this->input->post("analysis_name")));

			//$this->data['alert'] = create_alert('success', 'Treatment is added succesfully!');
		}
		else{
			//$this->data['alert'] = create_alert('danger', 'The specified visit was not found!');
		}

		redirect("doctor/visit/" . $visit_id);
	}

	public function weeklySchedule(){
		$this->data['week_days'] = get_week_days();

		$this->load->model('working_hours_model', '', TRUE);

		$this->data['weekly_schedule'] = $this->working_hours_model->get_weekly_schedule($this->userData['id']);

		$alert = $this->input->get('alert');
		
		if(isset($alert)){
			if($alert == '1'){
				$this->data['alert'] = create_alert('danger', 'Please fill the form completely');
			}
			else if($alert == '2'){
				$this->data['alert'] = create_alert('danger', 'Please enter a valid capacity');	
			}
			else if($alert == '3'){
				$this->data['alert'] = create_alert('danger', 'The time period you are trying to add is overlapping another scheduled work period.');
			}
			else if($alert == '4'){
				$this->data['alert'] = create_alert('success', 'You have succesfully added a new work period.');
			}
		}

		$this->load->view('doctor/weeklyschedule', $this->data);
	}

	public function addWorkingHour(){

		$day = $this->input->post('day');
		$start_time = $this->input->post('start_time');
		$end_time = $this->input->post('end_time');
		$capacity = $this->input->post('capacity');

		if( !isset($day) || !isset($start_time) || !isset($end_time) || !isset($capacity)){
			redirect('doctor/weeklySchedule?alert=1');
		}

		if( !is_numeric($capacity) || intval($capacity) < 0 || intval($capacity) > 100 ){
			redirect('doctor/weeklySchedule?alert=2');
		}

		$this->load->model('working_hours_model', '', TRUE);

		$is_overlapping = $this->working_hours_model->is_overlapping($this->userData['id'], $day, $start_time, $end_time);

		if($is_overlapping){
			redirect('doctor/weeklySchedule?alert=3');
		}

		$this->working_hours_model->insert_working_hour(array("u_id" => $this->userData['id'],
															  "day" => $day,
															  "start_time" => $start_time,
															  "end_time" => $end_time,
															  "capacity" => $capacity));

		redirect('doctor/weeklySchedule?alert=4');
	}

	public function cancelWorkingHour($id){

		$this->load->model('working_hours_model', '', TRUE);
		$this->working_hours_model->cancel_working_hour($id);

		redirect('doctor/weeklySchedule');
	}

	public function appointments(){

		$this->load->model('appointment_model', '', TRUE);
	}

}