<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manager_controller extends MY_Controller {

	function __construct()
	{
	    parent::__construct();

	    if( $this->userData['userType'] != 'm' ){
			redirectToHome($this->userData['userType']);
		}

		$this->data['meta'] = "";

		loadJs("jquery-1.11.1.min", $this->data['meta']);
		loadJs("ui", $this->data['meta']);
		loadJs("tabs", $this->data['meta']);
		loadCss("style", $this->data['meta']);
		loadCss('basicgrey', $this->data['meta']);
		loadCss('bootstrap.min', $this->data['meta']);
		loadCss('bootstrap-theme.min', $this->data['meta']);
		loadCss("tabs", $this->data['meta']);
	}

	public function financialReport() {

		$this->load->model( "patient_model", "patient", TRUE);

        $sql = "SELECT * FROM (SELECT SUM(debt) as total_debt FROM (SELECT Count(*) * Insurance_type.exam_price as debt, v_id, Visit.p_id FROM Examination, Visit, Patient, Insurance_type WHERE Examination.v_id = Visit.visit_id AND Visit.p_id = Patient.patient_id AND Patient.insurance_id = Insurance_type.insurance_id GROUP BY v_id) debt_table) total_debt_table, (SELECT SUM(amount) as total_payment FROM Payment) total_payment_table";
        $query = $this->db->query($sql);

        $result_array = $query->result_array();

        $this->data['result'] = $result_array[0];
        $this->load->view( 'manager/financialreport', $this->data);
	}

	public function doctorReport() {

		$this->load->model( "patient_model", "patient", TRUE);

        $sql = "SELECT Count(Distinct e_id) as examination_count, Count(Distinct Patient.patient_id) as patient_count, User.name, User.surname FROM Examination, Visit NATURAL JOIN Doctor, Patient, User WHERE Examination.v_id = Visit.visit_id AND Doctor.d_id=User.user_id AND Visit.p_id = Patient.patient_id GROUP BY d_id";
        $query = $this->db->query($sql);

        $result_array = $query->result_array();

        $this->data['results'] = $result_array;
        $this->load->view( 'manager/doctorreport', $this->data);

	}

	public function index()
	{

		redirect( 'manager/searchUser');
	}

	public function searchUser() {

		$this->data["searchDone"] = false;
		$this->load->model( "user_model", "user", TRUE);

		if( !empty( $_POST['search'])) {

			$keyword = $_POST['search'];

			$this->data["allUsers"] = $this->user->get_users( $keyword);
			$this->data["searchDone"] = true;
			$this->data["keyword"] = $keyword;
		}

		else {

			$this->data["allUsers"] = $this->user->get_all_users( $this->userData['id']);
		}

		$this->load->view( 'manager/searchuser.php', $this->data);
	}

	public function browseOffice() {

		$this->load->model( "room_model", "office", true);
		$this->data['error'] = false;
		$this->data['update'] = false;

		if( !empty($_GET['delete'])) {

			$deleteId = $_GET['delete'];
			if( $this->office->is_office_empty( $deleteId)) {

				$this->office->delete_room( $deleteId);
			}

			else {

				$this->data['error'] = true;
				$this->data['errorMessage'] = "Doctors associated with that office should be moved, before deleting the office";
			}
		}

		if( !empty($_GET['update']) && !empty($_POST['updateSumit'])) {
			echo $_GET['update'] . "-" . $_POST['updateSubmit'] . "-" . $_POST['updateNumber'];
			exit();
		}
		if( !empty($_GET['update']) && !empty($_POST['updateSubmit']) && !empty($_POST['updateNumber'])) {

			$updateId = $_GET['update'];
			$updateNumber = $_POST['updateNumber'];
			$updateLocation = $_POST['updateLocation'];

			if( !$this->office->update_office($updateId, array( $updateNumber, $updateLocation))) {

				$this->data['error'] = true;
				$this->data['errorMessage'] = "There is already an office with the same number";
			}
		}

		else if( !empty($_GET['update'])) {

			$updateId = $_GET['update'];
			$this->data['update'] = true;

			$this->data['updateOffice'] = $this->office->get_room( $updateId)->as_array();
		}

		$this->data['offices'] = $this->office->get_all_offices();

		$this->load->view( 'manager/browseoffice', $this->data);
	}

	public function addOffice() {

		$error = false;
		$errorMessage = "";
		$this->data['update'] = false;

		$this->load->model( "room_model", "office", true);
		$this->data['offices'] = $this->office->get_all_offices();
		if( empty( $_POST['name'])) {

			$error = true;
			$errorMessage = "You must fill name field";
		}

		else {
			$name = $_POST['name'];
			$location = $_POST['location'];

			$result = $this->office->get_room_with_number( $name);

			if( $result) {

				$error = true;
				$errorMessage = "There is already an office with the same number";
			}

			else {

				$this->office->insert_room( array( 'number' => $name, 'location' => $location), true);
				redirect( "manager/browseOffice");
			}
		}

		$this->data['error'] = $error;
		$this->data['errorMessage'] = $errorMessage;
		$this->data['filled'] = true;
		$this->load->view( 'manager/browseoffice', $this->data);
	}

	public function browseSickroom() {

		$this->load->model( "room_model", "sickroom", true);
		$this->data['error'] = false;
		$this->data['update'] = false;

		if( !empty($_POST['submit'])) {

			if( empty( $_POST['name']) || empty( $_POST['capacity'])) {

				$error = true;
				$errorMessage = "You must fill name and capacity field";
			}

			else {
				$name = $_POST['name'];
				$location = $_POST['location'];
				$capacity = intval( $_POST['capacity']);

				$result = $this->sickroom->get_room_with_number( $name);

				if( $result) {

					$error = true;
					$errorMessage = "There is already an office with the same number";
				}

				else if( $capacity <= 0) {

					$error = true;
					$errorMessage = "Please enter a valid capacity value.";
				}

				else {

					$this->sickroom->insert_room( array( 'number' => $name, 'location' => $location, 'capacity' => $capacity), false);
				}
			}
		}

		if( !empty($_GET['delete'])) {

			$deleteId = $_GET['delete'];
			if( $this->sickroom->is_sickroom_empty( $deleteId)) {

				$this->sickroom->delete_sickroom( $deleteId);
			}

			else {

				$this->data['error'] = true;
				$this->data['errorMessage'] = "Patients associated with that sickroom should be moved, before deleting the sickroom";
			}
		}

		if( !empty($_GET['update']) && !empty($_POST['updateSumit'])) {
			echo $_GET['update'] . "-" . $_POST['updateSubmit'] . "-" . $_POST['updateNumber'];
			exit();
		}
		if( !empty($_GET['update']) && !empty($_POST['updateSubmit']) && !empty($_POST['updateNumber'])) {

			$updateId = $_GET['update'];
			$updateNumber = $_POST['updateNumber'];
			$updateLocation = $_POST['updateLocation'];
			$updateCapacity = intval( $_POST['updateCapacity']);

			if( $updateCapacity <= 0) {

				$this->data['error'] = true;
				$this->data['errorMessage'] = "Please enter a valid capacity number";
			}

			else if( !$this->sickroom->update_sickroom($updateId, array( $updateNumber, $updateLocation, $updateCapacity))) {

				$this->data['error'] = true;
				$this->data['errorMessage'] = "There is already a sickroom with the same number";
			}
		}

		else if( !empty($_GET['update'])) {

			$updateId = $_GET['update'];
			$this->data['update'] = true;

			$this->data['updateSickroom'] = $this->sickroom->get_sickroom( $updateId);
		}

		$this->data['sickrooms'] = $this->sickroom->get_all_sickrooms();

		$this->load->view( 'manager/browsesickroom', $this->data);
	}

	public function browseDepartment() {

		$this->data['error'] = false;

		$this->load->model( "department_model", "department", true);

		if( !empty( $_POST['name'])) {

			$name = $_POST['name'];
			if( $this->department->get_department_with_name( $name)) {

				$this->data['error'] = true;
				$this->data['errorMessage'] = "$name department already exists";
			}

			else {

				$this->department->insert_department( $name);
			}
		}

		if( !empty( $_GET['delete'])) {

			$deleteId = $_GET['delete'];

			$sql = "SELECT Department.department_id FROM Department, Doctor, User WHERE User.user_id = Doctor.d_id AND User.active = 1 AND Department.department_id=Doctor.department_id AND Department.department_id=?";

			$query = $this->db->query( $sql, array( $deleteId));

			if( $query->num_rows) {


				$this->data['error'] = true;
				$this->data['errorMessage'] = "All related doctors should be moved before deleting a department.";
			}

			else {

				$this->department->delete_department( $deleteId);
			}
			
		}

		$this->data['departments'] = $this->department->get_all_departments();
		$this->load->view( 'manager/browsedepartment', $this->data);
	}

	//USER FUNCTIONS
	public function addUser() {

		$this->load->model( "user_model", "user", true);
		$this->data['filled'] = false;
		$this->data["update_request"] = false;
		if( !empty( $_POST['submit'])) {

			$this->data['filled'] = true;

			$username = $_POST['username'];
			$name = $_POST['name'];
			$surname = $_POST['surname'];
			$email = $_POST['email'];
			$type = $_POST['type'];
			$title = $_POST['title'];
			$office = $_POST['office'];
			$department = $_POST['department'];
			$password = $_POST['password'];

			if( (empty($username) || empty($name) || empty($surname) || empty($email) || empty($type)
								 || empty($password) ||
				( $type=="Doctor" && (empty($department) || empty( $title))))) {

				$this->data['error'] = true;
				$this->data['errorMessage'] = "Please fill the form completely";

				$this->data['username'] = $username;
				$this->data['name'] = $name;
				$this->data['surname'] = $surname;
				$this->data['email'] = $email;
				$this->data['type'] = $type;
				$this->data['title'] = $title;
				$this->data['office'] = $office;
				$this->data['department'] = $department;
				$this->data['password'] = $password;
			}

			else if( $this->user->is_username_exist( $username)) {

				$this->data['error'] = true;
				$this->data['errorMessage'] = "That username has taken by someone else";

				$this->data['username'] = $username;
				$this->data['name'] = $name;
				$this->data['surname'] = $surname;
				$this->data['email'] = $email;
				$this->data['type'] = $type;
				$this->data['title'] = $title;
				$this->data['office'] = $office;
				$this->data['department'] = $department;
				$this->data['password'] = $password;
			}

			else {

				$insertData['username'] = $username;
				$insertData['name'] = $name;
				$insertData['surname'] = $surname;
				$insertData['email'] = $email;
				$insertData['type'] = $type;
				$insertData['title'] = $title;
				$insertData['r_id'] = $office;
				$insertData['department'] = $department;
				$insertData['password'] = $password;

				$this->user->insert_user( $insertData);
				redirect( "manager/searchUser");
			}
		}

		$this->load->model( "department_model", "department", true);
		$this->load->model( "room_model", "office", true);

		$this->data["departments"] = $this->department->get_all_departments();
		$this->data["offices"] = $this->office->get_all_offices();
		$this->load->view( 'manager/adduser', $this->data);
	}

	public function deleteUser() {

		if( !empty( $_GET['user_id'])) {

			$userId = intval( $_GET['user_id']);
			$this->load->model( "user_model", "user", true);
			$this->user->delete_user( $userId);
		}
		redirect( 'manager/searchUser');
	}

	public function updateUser() {

		$this->load->model( "user_model", "user", true);
		$this->data['error'] = false;
		$updateId = $_GET['user_id'];
		$this->data['filled'] = true;

		if( !empty( $_POST['updateSubmit'])) {

			$name = $_POST['name'];
			$surname = $_POST['surname'];
			$email = $_POST['email'];
			$title = $_POST['title'];
			$office = $_POST['office'];
			$department = $_POST['department'];

			$sql = "SELECT type FROM User WHERE user_id=".$updateId." LIMIT 1";
			$query = $this->db->query( $sql);
			$type = $query -> result_array();
			$type = $type[0];

			$sql = "UPDATE User SET name=?, surname=?, email=? WHERE user_id=" . $updateId;
			$this->db->query( $sql, array( $name, $surname, $email));

			if( $type="d") {

				$sql = "UPDATE Doctor SET title=?, r_id=?, department_id=? WHERE d_id=" . $updateId;
				$this->db->query( $sql, array( $title, $office, $department));
			}

			redirect( 'manager/searchUser');
		}

		$this->data["update_request"] = true;
		$this->data["update"] = $this->user->get_user( $updateId)->as_array();

		$this->data["update"]['department_id'] = "";
		$this->data["update"]['title'] = "";
		$this->data["update"]['r_id'] = "";

		if( $this->data["update"]["type"] == "d") {
			$results = $this->user->get_doctor( $updateId);



			$this->data["update"]['department_id'] = $results['department_id'];
			$this->data["update"]['title'] = $results['title'];
			$this->data["update"]['r_id'] = $results['r_id'];
		}

		$this->load->model( "department_model", "department", true);
		$this->load->model( "room_model", "office", true);

		$this->data["update"]["departments"] = $this->department->get_all_departments();
		$this->data["update"]["offices"] = $this->office->get_all_offices();

		$this->load->view( 'manager/adduser.php', $this->data);
	}
}