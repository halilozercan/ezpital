<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_controller extends MY_Login_Controller {

	public function index()
	{
		if( $this->input->post('username') ){
			$this->check_login();
		}

		$data['meta'] = "";

		loadJs("jquery-1.11.1.min", $data['meta']);
		loadJs("ui", $data['meta']);
		loadCss("style-login", $data['meta']);
		loadCss("basicgrey", $data['meta']);
		
		
		$this->load->view('login', $data);

	}

	private function check_login(){

		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$sql = "SELECT * FROM User WHERE username = ? AND password = ? AND active=1"; 

		$query = $this->db->query($sql, array($username, $password));

		if($query->num_rows()){
			$user = $query->row();
			$userData = array("id" => $user->user_id, "userType" => $user->type, "username" => $user->username, "name" => $user->name . " " . $user->surname);
			$this->session->set_userdata($userData);
			redirect('/');

		}
		else{
			$this->data['login_fault'] = 1;
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect("/");
	}
}