<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Secretary_controller extends MY_Controller {

	function __construct()
	{
	    parent::__construct();

	    if( $this->userData['userType'] != 's' ){
			redirectToHome($this->userData['userType']);
		}

		$this->data['meta'] = "";

		loadJs("jquery-1.11.1.min", $this->data['meta']);
		loadJs("ui", $this->data['meta']);
		loadJs("tabs", $this->data['meta']);
		loadCss("style", $this->data['meta']);
		loadCss('basicgrey', $this->data['meta']);
		loadCss('bootstrap.min', $this->data['meta']);
		loadCss('bootstrap-theme.min', $this->data['meta']);
		loadCss("tabs", $this->data['meta']);

	}

	public function index()
	{
		$this->data['secretaryName'] = $this->userData['name'];
		$this->data['fullname'] = $this->userData['name'];


		$this->load->view('secretary/index', $this->data);

	}

	public function findPatient(){

		$this->data["searchDone"] = FALSE;

		if($this->input->get("submit")){

			$name = $this->input->get("name");
			$surname = $this->input->get("surname");
			$inpatient = $this->input->get("inpatient");

			$this->load->model("patient_model", "patient", TRUE);

			$this->data["searchResult"] = $this->patient->find_patients($name, $surname, $inpatient);
			$this->data["searchDone"] = TRUE;
			$this->data["searchName"] = $name;
			$this->data["searchSurname"] = $surname;
			$this->data["searchInpatient"] = $inpatient;

		}

		$this->load->view('secretary/findpatient', $this->data);
	}

	public function addPatient(){

		if($this->input->post("nationalid")){

			$this->load->model("patient_model", "patient", TRUE);

			$this->patient->insert_patient(array("nationalid" => $this->input->post("nationalid"),
												 "patient_name" => $this->input->post("name"),
												 "patient_surname" => $this->input->post("surname"),
												 "birthdate" => $this->input->post("birthdate"),
												 "bloodtype" => $this->input->post("bloodtype"),
												 "insurancetype" => $this->input->post("insurancetype"),
												 "insurancenumber" => $this->input->post("insurancenumber"),
												 "phone" => $this->input->post("phone"),
												 "address" => $this->input->post("address") ) );

			$this->data['alert'] = array('type' => 'success', 'message' => 'Patient added to the system successfully');
		}

		$this->load->model("insurance_model", "insurance", TRUE);

		$this->data['bloodtypes'] = getBloodTypes();
		$this->data['insurancetypes'] = $this->insurance->get_insurance_types();

		$this->load->view('secretary/addpatient', $this->data);
	}

	public function addPayment(){

		if($this->input->get("visit_id")){

			$this->load->model("visit_model", "", TRUE);
			$mVisit = $this->visit_model->get_visit($this->input->get("visit_id"));

			if( isset($mVisit->visit_id) ){
				$this->load->model("payment_model", "", TRUE);

				$data = array("v_id" => $this->input->get("visit_id"),
							  "amount" => $this->input->get("amount"),
							  "currency" => "TL");

				$this->payment_model->insert_payment($data);
			}
		}

		redirect('secretary/visits');
	}

	public function searchVisits(){
		if($this->input->is_ajax_request()){

			$this->load->model("visit_model", "", TRUE);

			$data["patient_name"] = $this->input->get('patient_name'); 
			$data["patient_surname"] = $this->input->get('patient_surname');
			$data["start_date"] = $this->input->get('start_date');
			$data["end_date"] = $this->input->get('end_date');

			echo json_encode($this->visit_model->search_visits($data));
			exit;
		}
	}

	public function visits(){
		$this->load->model("visit_model", "", TRUE);

		$this->data['debt_visits'] = $this->visit_model->get_visits_with_debts();

		$this->load->view('secretary/visits', $this->data);
	}

	public function viewPatient($patient_id){
		$alert = $this->input->get('alert');
		if($alert == '1'){
			$this->data['alert'] = create_alert('danger', 'There are no available doctors right now!');
		}
		else if($alert == '2'){
			$this->data['alert'] = create_alert('danger', 'There is already an open visit for the patient!');
		}

		$this->load->model("patient_model", "patient", TRUE);
		$this->load->model("visit_model", "visit", TRUE);
		$this->load->model("department_model", "department", TRUE);
		$this->data['found'] = FALSE;

		$mPatient = $this->patient->get_patient($patient_id);
		$mVisits = $this->visit->get_visits_patient($patient_id);
		
		if( isset($mPatient->national_id) ){
			$this->data['found'] = TRUE;
			$this->data['patient'] = $mPatient->as_array();
			$this->data['visits'] = $mVisits;
		}
		else{
			$this->data['alert'] = create_alert('danger', 'The specified patient was not found!');
		}
		
		$this->data["departments"] = $this->department->get_all_departments();

		$this->load->view('secretary/viewpatient', $this->data);
	}

	public function addVisit($patient_id){

		$this->load->model("visit_model", "", TRUE);
		$this->load->model("patient_model", "", TRUE);
		$this->load->model("doctor_model", "", TRUE);

		$mPatient = $this->patient_model->get_patient($patient_id);

		if( isset($mPatient->national_id) ){

			$is_open_visit = $this->visit_model->is_there_open_visit($mPatient->patient_id);

			if($is_open_visit){
				redirect('secretary/viewpatient/' . $patient_id . '?alert=2');
			}

			$available_doctors = $this->doctor_model->get_available_doctors($this->input->post('department'));

			if(empty($available_doctors)){
				redirect('secretary/viewpatient/' . $patient_id . '?alert=1');
			}

			$doctor = $available_doctors[rand(0,count($available_doctors)-1)];

			$visit_id = $this->visit_model->insert_visit(array("reason" => $this->input->post('reason'),
															 "p_id" => $patient_id,
												   			 "d_id" => $doctor->doctor_id));

		}
		redirect('secretary/viewpatient/' . $patient_id );
	}

	public function appointments(){

		$this->load->model('appointment_model', '', TRUE);

		$this->data["search_done"] = FALSE;

		if($this->input->get("submit")){

			$national_id = $this->input->get("national_id");
			$name = $this->input->get("name");
			$surname = $this->input->get("surname");

			$this->data["search_result"] = $this->appointment_model->find_appointments($name, $surname, $national_id);
			$this->data["search_done"] = TRUE;
			$this->data["search_name"] = $name;
			$this->data["search_surname"] = $surname;
			$this->data["search_national_id"] = $national_id;

		}

		$this->load->view('secretary/appointments', $this->data);

	}

	public function doneAppointment($appointment_id){

		$this->load->model('appointment_model', '', TRUE);

		$this->appointment_model->done_appointment($appointment_id);

		redirect('secretary/appointments');

	}
}