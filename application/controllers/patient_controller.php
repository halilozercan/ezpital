<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Patient_controller extends CI_Controller {
	function __construct() {
		parent::__construct ();
		
		$this->data ['meta'] = "";
		
		loadJs ( "jquery-1.11.1.min", $this->data ['meta'] );
		loadJs ( "ui", $this->data ['meta'] );
		loadJs ( "tabs", $this->data ['meta'] );
		loadCss ( "style", $this->data ['meta'] );
		loadCss ( 'basicgrey', $this->data ['meta'] );
		loadCss ( 'bootstrap.min', $this->data ['meta'] );
		loadCss ( 'bootstrap-theme.min', $this->data ['meta'] );
		loadCss ( "tabs", $this->data ['meta'] );
	}
	public function index() {
		$this->data ['patientName'] = $this->userData ['name'];
		
		$this->load->view ( 'patient/index', $this->data );
	}
	public function requestAppointment() {
		
		if($this->input->is_ajax_request()){

			$a_name = $this->input->get('name');
			$a_surname = $this->input->get('surname');
			$national_id = $this->input->get('nationalid');
			$date = $this->input->get('date');
			$time = $this->input->get('time');
			$department_id = $this->input->get('department_id');

			if(empty($a_name) || empty($a_surname) || empty($national_id) || empty($date) || empty($time) || empty($department_id)){
				echo json_encode(array("condition" => "danger", "message" => "Please fill all the fields."));
				exit;
			}

			$this->load->model('appointment_model', '', TRUE);

			$another_appointment = $this->appointment_model->any_other_appointment($department_id, $date, $national_id);

			if($another_appointment > 0){
				echo json_encode(array("condition" => "danger", "message" => "You already have an appointment for this day and department!"));
				exit;
			}

			$doctor = $this->appointment_model->is_appointment_available($department_id, $date, $time);

			if($doctor){

				$appointment_id = $this->appointment_model->insert_appointment(array("a_name" => $a_name,
																   "a_surname" => $a_surname,
																   "national_id" => $national_id,
																   "date" => $date,
																   "time" => $time,
																   "d_id" => $doctor->doctor_id));

				if($appointment_id){
					echo json_encode(array("condition" => "success", "message" => "You have successfully made an appointment. Date: " . $date . " Time: " . $time . " Doctor: " . $doctor->name . " " . $doctor->surname));
				}
				else{
					echo json_encode(array("condition" => "danger", "message" => "A problem occurred while processing your request!"));	
				}
			}
			else{
				echo json_encode(array("condition" => "danger", "message" => "There are no available doctors for your appointment. Please try again."));	
			}
			exit;
		}

		$this->load->model ( "department_model", "department", TRUE );
		$this->data ['departments'] = $this->department->get_all_departments ();


		$this->load->view ( 'patient/requestappointment', $this->data );
	}
}