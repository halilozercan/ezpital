<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Nurse_controller extends MY_Controller {

	function __construct() {
		parent::__construct ();
		
		if ($this->userData ['userType'] != 'n') {
			redirectToHome ( $this->userData ['userType'] );
		}
		
		$this->data ['meta'] = "";
		
		loadJs ( "jquery-1.11.1.min", $this->data ['meta'] );
		loadJs ( "ui", $this->data ['meta'] );
		loadJs ( "tabs", $this->data ['meta'] );
		loadCss ( "style", $this->data ['meta'] );
		loadCss ( 'basicgrey', $this->data ['meta'] );
		loadCss ( 'bootstrap.min', $this->data ['meta'] );
		loadCss ( 'bootstrap-theme.min', $this->data ['meta'] );
		loadCss ( "tabs", $this->data ['meta'] );
	}

	public function index() {

		$this->load->model('nurse_model', '', TRUE);
		$mNurse = $this->nurse_model->get_nurse($this->userData['id']);
		$this->data['nurse'] = $mNurse;

		$this->data['analysesDoneToday'] = $mNurse->get_today_analyses_count();
		$this->data['waitingAnalyses'] = $mNurse->get_waiting_analyses_count();

		$this->load->view ( 'nurse/index', $this->data );
	}

	public function findPatient() {
		$this->data ["searchDone"] = FALSE;
		
		if ($this->input->get ( "submit" )) {
			
			$name = $this->input->get ( "name" );
			$surname = $this->input->get ( "surname" );
			$inpatient = $this->input->get ( "inpatient" );
			
			$this->load->model ( "patient_model", "patient", TRUE );
			
			$this->data ["searchResult"] = $this->patient->find_patients ( $name, $surname, $inpatient );
			$this->data ["searchDone"] = TRUE;
			$this->data ["searchName"] = $name;
			$this->data ["searchSurname"] = $surname;
			$this->data ["searchInpatient"] = $inpatient;
		}
		
		$this->load->view ( 'nurse/findpatient', $this->data );
	}

	public function viewPatient($patient_id) {
		$this->load->model ( "patient_model", "patient", TRUE );
		$this->load->model ( "visit_model", "visit", TRUE );
		$this->data ['found'] = FALSE;
		
		$mPatient = $this->patient->get_patient ( $patient_id );
		$mVisits = $this->visit->get_visits_patient ( $patient_id );
		
		if (isset ( $mPatient->national_id )) {
			$this->data ['found'] = TRUE;
			$this->data ['patient'] = $mPatient->as_array ();
			$this->data ['visits'] = $mVisits;
		} else {
			$this->data ['alert'] = create_alert ( 'danger', 'The specified patient was not found!' );
		}
		$this->load->view ( 'nurse/viewpatient', $this->data );
	}

	public function viewVisit($visit_id) {
		$this->load->model ( "analysis_model", "analysis", TRUE );
		$analyses = $this->analysis->get_analysises_visit_from_nurse ( $visit_id, $this->userData ['id'] );
		$this->data ['analyses'] = $analyses;
		$this->data ['visit_id'] = $visit_id;
		$this->load->view ( 'nurse/viewvisit', $this->data );
	}

	public function addAnalysis() {
		$this->load->model ( "analysis_model", "analysis", TRUE );

		$analysis_data ["analysisID"] = $this->input->post ("analysisID");
		$analysis_data ["report"] = $this->input->post ("report");

		$this->analysis->insert_analysis_nurse ( $analysis_data );
		$this->viewVisit ( $this->input->post ("visitID") );
	}

	public function myAnalyses(){

		$this->load->model("analysis_model", "", TRUE);

		$this->data["all_analyses"] = $this->analysis_model->get_all_analyses($this->userData['id']);
		$this->data["waiting_analyses"] = $this->analysis_model->get_waiting_analyses($this->userData['id']);

		$this->load->view('nurse/myanalyses', $this->data);
	}
}