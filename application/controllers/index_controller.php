<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index_controller extends MY_Controller {

	public function index()
	{
		
		redirectToHome($this->userData['userType']);

	}

}