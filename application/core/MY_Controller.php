<?php
class MY_Controller extends CI_Controller {

	function __construct()
	{
	    parent::__construct();

	    $this->data = array();
	    $this->userData = $this->session->all_userdata();

	    if( !isset($this->userData['id']) ){
			redirect('/login');
		}

		$this->data['userData']  =$this->userData;

	    prepare_header($this->data['headerData'], $this->userData);

	}

}

class MY_Login_Controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->data = array();
	}
}

?>