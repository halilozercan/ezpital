$( document ).ready(function() {
	
	$( "#openSideMenu").click( function() {

		$( "#sideMenu").toggleClass( "hidden");
	})

	$( "#openProfile").click( function() {

		$( "#profile").toggleClass( "hidden");
	})

	$( "#body").click( function() {

		$( "#sideMenu").addClass( "hidden");
		$( "#profile").addClass( "hidden");
	});
});